package com.example.eatapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.eatapp.user_flow.customer.Login.LoginDataModel;

import java.util.List;

@Dao
public interface LoginDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(LoginDataModel data);

    @Query("SELECT * FROM INFORMATIONUSER")
    List<LoginDataModel> getAll();


}