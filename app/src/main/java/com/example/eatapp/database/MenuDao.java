package com.example.eatapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.List;

@Dao
public interface MenuDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MenuItemModel order);
    //List<MenuItemModel> order
    //long[] insert(User... user);

    @Query("SELECT * FROM Menu")
    List<MenuItemModel> getOrder();

    @Query("SELECT * FROM Menu WHERE name = :name ")
    MenuItemModel loadSingle(String name);

    @Query("DELETE FROM Menu")
    public void deleteTable();

    @Query("DELETE FROM Menu WHERE name = :name")
    public void deleteItem(String name);



}
