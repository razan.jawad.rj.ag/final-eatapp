package com.example.eatapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.eatapp.user_flow.customer.Login.LoginDataModel;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

//1.add the pojo what we want to save in data base  her
@Database(entities = {LoginDataModel.class, MenuItemModel.class}, version = 1)
public abstract class LocalDatabase extends RoomDatabase {

    private static LocalDatabase INSTANCE;

    public abstract LoginDao daoUser();
    //2.add object from your interface
    public abstract MenuDao daoMenu ();

    public static LocalDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), LocalDatabase.class, "movie-database")
                            .build();
        }
        return INSTANCE;
    }
}
