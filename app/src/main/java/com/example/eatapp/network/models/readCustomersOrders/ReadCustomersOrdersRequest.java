package com.example.eatapp.network.models.readCustomersOrders;


import com.google.gson.annotations.SerializedName;


public class ReadCustomersOrdersRequest{

	@SerializedName("order_type_id")
	private String orderTypeId;

	public void setOrderTypeId(String orderTypeId){
		this.orderTypeId = orderTypeId;
	}

	public String getOrderTypeId(){
		return orderTypeId;
	}

	@Override
 	public String toString(){
		return 
			"ReadCustomersOrdersRequest{" + 
			"order_type_id = '" + orderTypeId + '\'' + 
			"}";
		}
}