package com.example.eatapp.network.models.ReadManagerDilevaryPrice;


import com.google.gson.annotations.SerializedName;


public class DeliveryPriceAndPhoneResponse{

	@SerializedName("Reading customer order details")
	private ReadingCustomerOrderDetails readingCustomerOrderDetails;

	@SerializedName("response")
	private Response response;

	public void setReadingCustomerOrderDetails(ReadingCustomerOrderDetails readingCustomerOrderDetails){
		this.readingCustomerOrderDetails = readingCustomerOrderDetails;
	}

	public ReadingCustomerOrderDetails getReadingCustomerOrderDetails(){
		return readingCustomerOrderDetails;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"DeliveryPriceAndPhoneResponse{" + 
			"reading customer order details = '" + readingCustomerOrderDetails + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}