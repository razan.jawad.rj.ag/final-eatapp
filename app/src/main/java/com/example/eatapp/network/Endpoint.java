package com.example.eatapp.network;

import android.arch.persistence.room.TypeConverters;

import com.example.eatapp.network.models.ReadManagerDilevaryPrice.DeliveryPriceAndPhoneRequst;
import com.example.eatapp.network.models.ReadManagerDilevaryPrice.DeliveryPriceAndPhoneResponse;
import com.example.eatapp.network.models.addLocation.AddLocationRequst;
import com.example.eatapp.network.models.addLocation.AddLocationResponse;
import com.example.eatapp.network.models.addOffer.AddOfferRequest;
import com.example.eatapp.network.models.addOffer.AddOfferResponse;
import com.example.eatapp.network.models.addReservationCustomer.AddResevationCustomerRequest;
import com.example.eatapp.network.models.addReservationCustomer.AddResevationCustomerResponse;
import com.example.eatapp.network.models.avaibilityTable.AvailabilityTableRequest;
import com.example.eatapp.network.models.avaibilityTable.AvailabilityTableResponse;
import com.example.eatapp.network.models.createAccount.CreateRequestsPojo;
import com.example.eatapp.network.models.createAccount.CreateResponsePojo;
import com.example.eatapp.network.models.getOffer.ReadOfferRequst;
import com.example.eatapp.network.models.getOffer.ReadOfferResponse;
import com.example.eatapp.network.models.getReservationDateTime.GetReservationDateTimeRequst;
import com.example.eatapp.network.models.getReservationDateTime.GetReservationDateTimeResponse;
import com.example.eatapp.network.models.login.LoginRequestsPojo;
import com.example.eatapp.network.models.login.LoginResponsePojo;
import com.example.eatapp.network.models.managerLogin.ManagerLoginRequst;
import com.example.eatapp.network.models.managerLogin.ManagerLoginResponse;
import com.example.eatapp.network.models.managerReadOrderDetails.OrderDetailsRequst;
import com.example.eatapp.network.models.managerReadOrderDetails.OrderDetailsResponse;
import com.example.eatapp.network.models.menu.MenuResponsePojo;
import com.example.eatapp.network.models.menu_details.MenuDetailsRequest;
import com.example.eatapp.network.models.menu_details.MenuDetailsResponsePojo;
import com.example.eatapp.network.models.readAllFavorite.ReadFavoriteResponse;
import com.example.eatapp.network.models.readCustomersOrders.ReadCustomersOrdersRequest;
import com.example.eatapp.network.models.readCustomersOrders.ReadCustomersOrdersResponse;
import com.example.eatapp.network.models.readDeliveryBill.ReadDeliveryBillRequest;
import com.example.eatapp.network.models.readDeliveryBill.ReadDeliveryBillResponse;
import com.example.eatapp.network.models.readReservationBill.ReservationBillReqest;
import com.example.eatapp.network.models.readReservationBill.ReservationBillResponse;
import com.example.eatapp.network.models.readRestaurantInf.ReadRetaurantinformationResponse;
import com.example.eatapp.network.models.read_tackaway_bill.ReadTackawayBillRequest;
import com.example.eatapp.network.models.read_tackaway_bill.ReadTackawayBillResponse;
import com.example.eatapp.network.models.updateFavorite.FavortRequest;
import com.example.eatapp.network.models.updateFavorite.FavortResponse;
import com.example.eatapp.network.models.updateOrder.UpdateOrderReqest;
import com.example.eatapp.network.models.updateOrder.UpdateOrderResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Endpoint {

    @POST("rest/Auction/CustomerLogin")
    Call<LoginResponsePojo> login(@Body LoginRequestsPojo loginRequestsPojo);

    @POST("rest/Auction/readMenu")
    Call<MenuResponsePojo> getMenu();


    @POST("rest/Auction/readMenuDetails")
    Call<MenuDetailsResponsePojo> getMenuDetail(@Body MenuDetailsRequest menuDetailsRequest);

    @POST("rest/Auction/AddCustomer")
    Call<CreateResponsePojo> addCustomer(@Body CreateRequestsPojo createRequestsPojo);


    @POST("rest/Auction/GetAvaliableReservation")
    Call<AvailabilityTableResponse> getAvailableTable(@Body AvailabilityTableRequest availabilityTableRequest);

    @POST("rest/Auction/updateFavorite")
    Call<FavortResponse> getFavort(@Body FavortRequest favortRequest);

    @POST("rest/Auction/AddReservationCustomer")
    Call<AddResevationCustomerResponse> addReservationCustomer(@Body AddResevationCustomerRequest addResevationCustomerRequest);

    @POST("rest/Auction/ReadReservationBill")
    Call<ReservationBillResponse> reservationBillResponse(@Body ReservationBillReqest reservationBillReqest);

    @POST("rest/Auction/ReadReservationCustomer")
    Call<GetReservationDateTimeResponse> getReservationDateTimeResponse(@Body GetReservationDateTimeRequst reservationBillReqest);

    @POST("rest/Auction/ReadAllFavorite")
    Call<ReadFavoriteResponse> readFavorite();


    @POST("rest/Auction/UpdateOrder")
    Call<UpdateOrderResponse> updateOrder(@Body List<UpdateOrderReqest> updateOrderReqests);

    @POST("rest/Auction/ReadTackawayBill")
    Call<ReadTackawayBillResponse> readTackawayBillResponseCall(@Body ReadTackawayBillRequest readTackawayBillRequest);

    @POST("rest/Auction/ReadRestInformation")
    Call<ReadRetaurantinformationResponse> readRetaurantininf();

    @POST("rest/Auction/ReadDeliveryBill")
    Call<ReadDeliveryBillResponse> readDeliveryBillResponse(@Body ReadDeliveryBillRequest readDeliveryBillRequest);


    @POST("rest/Admin/ReadCustomersOrders")
    Call<ReadCustomersOrdersResponse> readCustomersOrders(@Body ReadCustomersOrdersRequest readCustomersOrdersRequest);

    @POST("rest/Admin/ReadCustomerOrderDetails")
    Call<DeliveryPriceAndPhoneResponse> prcieAndPhoneDelivary(@Body DeliveryPriceAndPhoneRequst deliveryPriceAndPhoneRequst);

    @POST("rest/Admin/ResdOrderDetails")
    Call<OrderDetailsResponse> OrderDetails(@Body OrderDetailsRequst orderDetailsRequst);

    @POST("rest/Admin/AdminLogin")
    Call<ManagerLoginResponse> managerLogin(@Body ManagerLoginRequst managerLoginRequst);

    @POST("rest/Auction/ReadAllOffer")
    Call<ReadOfferResponse>readOffer(@Body ReadOfferRequst readOfferRequst);

    @POST("rest/Admin/UpdateOffer")
    Call<AddOfferResponse>addOffer(@Body AddOfferRequest addOfferRequest);

    @POST("rest/Auction/AddDeleviry")
    Call<AddLocationResponse>addLocation(@Body AddLocationRequst addLocationRequst);
//    @FormUrlEncoded
//    @POST("rest/Auction/GetAvaliableReservation")
//    Call<AvailabilityTableResponse>getAvailableTable(@Field("reservationStartDate") String reservationStartDate,
//                                                     @Field("personNum") String personNum);
}
