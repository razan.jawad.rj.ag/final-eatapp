package com.example.eatapp.network.models.avaibilityTable;


import com.google.gson.annotations.SerializedName;


public class AvailabilityTableRequest{

	@SerializedName("reservationStartDate")
	private String reservationStartDate;

	@SerializedName("personNum")
	private String personNum;

	public void setReservationStartDate(String reservationStartDate){
		this.reservationStartDate = reservationStartDate;
	}

	public String getReservationStartDate(){
		return reservationStartDate;
	}

	public void setPersonNum(String personNum){
		this.personNum = personNum;
	}

	public String getPersonNum(){
		return personNum;
	}

	@Override
 	public String toString(){
		return 
			"AvailabilityTableRequest{" + 
			"reservationStartDate = '" + reservationStartDate + '\'' + 
			",personNum = '" + personNum + '\'' + 
			"}";
		}
}