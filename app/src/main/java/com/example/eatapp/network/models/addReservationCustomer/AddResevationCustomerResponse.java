package com.example.eatapp.network.models.addReservationCustomer;


import com.google.gson.annotations.SerializedName;


public class AddResevationCustomerResponse{

	@SerializedName("response")
	private Response response;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"AddResevationCustomerResponse{" + 
			"response = '" + response + '\'' + 
			"}";
		}
}