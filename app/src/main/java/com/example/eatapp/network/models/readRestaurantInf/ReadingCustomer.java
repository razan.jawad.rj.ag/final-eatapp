package com.example.eatapp.network.models.readRestaurantInf;


import com.google.gson.annotations.SerializedName;


public class ReadingCustomer{

	@SerializedName("country")
	private String country;

	@SerializedName("city")
	private String city;

	@SerializedName("phone")
	private String phone;

	@SerializedName("restaurant_name_en")
	private String restaurantNameEn;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setRestaurantNameEn(String restaurantNameEn){
		this.restaurantNameEn = restaurantNameEn;
	}

	public String getRestaurantNameEn(){
		return restaurantNameEn;
	}

	@Override
 	public String toString(){
		return 
			"ReadingCustomer{" + 
			"country = '" + country + '\'' + 
			",city = '" + city + '\'' + 
			",phone = '" + phone + '\'' + 
			",restaurant_name_en = '" + restaurantNameEn + '\'' + 
			"}";
		}
}