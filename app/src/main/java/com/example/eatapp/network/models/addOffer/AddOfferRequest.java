package com.example.eatapp.network.models.addOffer;


import com.google.gson.annotations.SerializedName;


public class AddOfferRequest{

	@SerializedName("offer_id")
	private String offerId;

	@SerializedName("offer_price")
	private String offerPrice;

	public void setOfferId(String offerId){
		this.offerId = offerId;
	}

	public String getOfferId(){
		return offerId;
	}

	public void setOfferPrice(String offerPrice){
		this.offerPrice = offerPrice;
	}

	public String getOfferPrice(){
		return offerPrice;
	}

	@Override
 	public String toString(){
		return 
			"AddOfferRequest{" + 
			"offer_id = '" + offerId + '\'' + 
			",offer_price = '" + offerPrice + '\'' + 
			"}";
		}
}