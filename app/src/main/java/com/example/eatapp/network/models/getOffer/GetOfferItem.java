package com.example.eatapp.network.models.getOffer;

import com.google.gson.annotations.SerializedName;


public class GetOfferItem{

	@SerializedName("end_date")
	private String endDate;

	@SerializedName("menu_item_id")
	private String menuItemId;

	@SerializedName("offer_id")
	private String offerId;

	@SerializedName("start_date")
	private String startDate;

	@SerializedName("offer_price")
	private String offerPrice;

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setMenuItemId(String menuItemId){
		this.menuItemId = menuItemId;
	}

	public String getMenuItemId(){
		return menuItemId;
	}

	public void setOfferId(String offerId){
		this.offerId = offerId;
	}

	public String getOfferId(){
		return offerId;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}

	public void setOfferPrice(String offerPrice){
		this.offerPrice = offerPrice;
	}

	public String getOfferPrice(){
		return offerPrice;
	}

	@Override
 	public String toString(){
		return 
			"GetOfferItem{" + 
			"end_date = '" + endDate + '\'' + 
			",menu_item_id = '" + menuItemId + '\'' + 
			",offer_id = '" + offerId + '\'' + 
			",start_date = '" + startDate + '\'' + 
			",offer_price = '" + offerPrice + '\'' + 
			"}";
		}
}