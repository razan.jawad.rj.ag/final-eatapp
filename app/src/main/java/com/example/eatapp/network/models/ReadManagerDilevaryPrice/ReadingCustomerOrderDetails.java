package com.example.eatapp.network.models.ReadManagerDilevaryPrice;


import com.google.gson.annotations.SerializedName;


public class ReadingCustomerOrderDetails{

	@SerializedName("final_price")
	private String finalPrice;

	@SerializedName("phone")
	private String phone;

	public void setFinalPrice(String finalPrice){
		this.finalPrice = finalPrice;
	}

	public String getFinalPrice(){
		return finalPrice;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	@Override
 	public String toString(){
		return 
			"ReadingCustomerOrderDetails{" + 
			"final_price = '" + finalPrice + '\'' + 
			",phone = '" + phone + '\'' + 
			"}";
		}
}