package com.example.eatapp.network.models.readDeliveryBill;


import com.google.gson.annotations.SerializedName;


public class ReadDeliveryBillResponse{

	@SerializedName("Reading Bill")
	private ReadingBill readingBill;

	@SerializedName("response")
	private Response response;

	public void setReadingBill(ReadingBill readingBill){
		this.readingBill = readingBill;
	}

	public ReadingBill getReadingBill(){
		return readingBill;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"ReadDeliveryBillResponse{" + 
			"reading Bill = '" + readingBill + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}