package com.example.eatapp.network.models.readAllFavorite;


import com.google.gson.annotations.SerializedName;


public class GetFavoriteDetailsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("menu_item_id")
	private String menuItemId;

	@SerializedName("status_id")
	private int statusId;

	@SerializedName("item_name_ar")
	private String itemNameAr;

	@SerializedName("cooking_time")
	private String cookingTime;

	@SerializedName("item_name_en")
	private String itemNameEn;

	@SerializedName("item_description")
	private String itemDescription;

	@SerializedName("price_item")
	private String priceItem;

	@SerializedName("favorite")
	private String favorite;

	@SerializedName("menu_id")
	private String menuId;

	@SerializedName("offer_id")
	private int offerId;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setMenuItemId(String menuItemId){
		this.menuItemId = menuItemId;
	}

	public String getMenuItemId(){
		return menuItemId;
	}

	public void setStatusId(int statusId){
		this.statusId = statusId;
	}

	public int getStatusId(){
		return statusId;
	}

	public void setItemNameAr(String itemNameAr){
		this.itemNameAr = itemNameAr;
	}

	public String getItemNameAr(){
		return itemNameAr;
	}

	public void setCookingTime(String cookingTime){
		this.cookingTime = cookingTime;
	}

	public String getCookingTime(){
		return cookingTime;
	}

	public void setItemNameEn(String itemNameEn){
		this.itemNameEn = itemNameEn;
	}

	public String getItemNameEn(){
		return itemNameEn;
	}

	public void setItemDescription(String itemDescription){
		this.itemDescription = itemDescription;
	}

	public String getItemDescription(){
		return itemDescription;
	}

	public void setPriceItem(String priceItem){
		this.priceItem = priceItem;
	}

	public String getPriceItem(){
		return priceItem;
	}

	public void setFavorite(String favorite){
		this.favorite = favorite;
	}

	public String getFavorite(){
		return favorite;
	}

	public void setMenuId(String menuId){
		this.menuId = menuId;
	}

	public String getMenuId(){
		return menuId;
	}

	public void setOfferId(int offerId){
		this.offerId = offerId;
	}

	public int getOfferId(){
		return offerId;
	}

	@Override
 	public String toString(){
		return 
			"GetFavoriteDetailsItem{" + 
			"image = '" + image + '\'' + 
			",menu_item_id = '" + menuItemId + '\'' + 
			",status_id = '" + statusId + '\'' + 
			",item_name_ar = '" + itemNameAr + '\'' + 
			",cooking_time = '" + cookingTime + '\'' + 
			",item_name_en = '" + itemNameEn + '\'' + 
			",item_description = '" + itemDescription + '\'' + 
			",price_item = '" + priceItem + '\'' + 
			",favorite = '" + favorite + '\'' + 
			",menu_id = '" + menuId + '\'' + 
			",offer_id = '" + offerId + '\'' + 
			"}";
		}
}