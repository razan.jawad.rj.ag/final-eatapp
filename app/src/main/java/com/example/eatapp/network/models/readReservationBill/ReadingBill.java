package com.example.eatapp.network.models.readReservationBill;


import com.google.gson.annotations.SerializedName;


public class ReadingBill{

	@SerializedName("country")
	private String country;

	@SerializedName("final_price")
	private String finalPrice;

	@SerializedName("city")
	private String city;

	@SerializedName("phone")
	private String phone;

	@SerializedName("discount")
	private String discount;

	@SerializedName("restaurant_name_en")
	private String restaurantNameEn;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("price_item")
	private String priceItem;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setFinalPrice(String finalPrice){
		this.finalPrice = finalPrice;
	}

	public String getFinalPrice(){
		return finalPrice;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDiscount(String discount){
		this.discount = discount;
	}

	public String getDiscount(){
		return discount;
	}

	public void setRestaurantNameEn(String restaurantNameEn){
		this.restaurantNameEn = restaurantNameEn;
	}

	public String getRestaurantNameEn(){
		return restaurantNameEn;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setPriceItem(String priceItem){
		this.priceItem = priceItem;
	}

	public String getPriceItem(){
		return priceItem;
	}

	@Override
 	public String toString(){
		return 
			"ReadingBill{" + 
			"country = '" + country + '\'' + 
			",final_price = '" + finalPrice + '\'' + 
			",city = '" + city + '\'' + 
			",phone = '" + phone + '\'' + 
			",discount = '" + discount + '\'' + 
			",restaurant_name_en = '" + restaurantNameEn + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",price_item = '" + priceItem + '\'' + 
			"}";
		}
}