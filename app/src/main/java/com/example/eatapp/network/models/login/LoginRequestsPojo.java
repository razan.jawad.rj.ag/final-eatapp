package com.example.eatapp.network.models.login;


import com.google.gson.annotations.SerializedName;


public class LoginRequestsPojo {

	@SerializedName("password")
	private String password;

	@SerializedName("user_name")
	private String userName;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	@Override
 	public String toString(){
		return 
			"LoginRequestsPojo{" +
			"password = '" + password + '\'' + 
			",user_name = '" + userName + '\'' + 
			"}";
		}
}