package com.example.eatapp.network.models.readAllFavorite;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class ReadFavoriteResponse{

	@SerializedName("getFavorite_Details")
	private List<GetFavoriteDetailsItem> getFavoriteDetails;

	@SerializedName("response")
	private Response response;

	public void setGetFavoriteDetails(List<GetFavoriteDetailsItem> getFavoriteDetails){
		this.getFavoriteDetails = getFavoriteDetails;
	}

	public List<GetFavoriteDetailsItem> getGetFavoriteDetails(){
		return getFavoriteDetails;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"ReadFavoriteResponse{" + 
			"getFavorite_Details = '" + getFavoriteDetails + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}