package com.example.eatapp.network.models.managerReadOrderDetails;


import com.google.gson.annotations.SerializedName;


public class OrderDetailsResponse{

	@SerializedName("response")
	private Response response;

	@SerializedName("Reading Order")
	private ReadingOrder readingOrder;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setReadingOrder(ReadingOrder readingOrder){
		this.readingOrder = readingOrder;
	}

	public ReadingOrder getReadingOrder(){
		return readingOrder;
	}

	@Override
 	public String toString(){
		return 
			"OrderDetailsResponse{" + 
			"response = '" + response + '\'' + 
			",reading Order = '" + readingOrder + '\'' + 
			"}";
		}
}