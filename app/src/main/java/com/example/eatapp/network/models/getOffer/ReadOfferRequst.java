package com.example.eatapp.network.models.getOffer;


import com.google.gson.annotations.SerializedName;


public class ReadOfferRequst{

	@SerializedName("offer_id")
	private String offerId;

	public void setOfferId(String offerId){
		this.offerId = offerId;
	}

	public String getOfferId(){
		return offerId;
	}

	@Override
 	public String toString(){
		return 
			"ReadOfferRequst{" + 
			"offer_id = '" + offerId + '\'' + 
			"}";
		}
}