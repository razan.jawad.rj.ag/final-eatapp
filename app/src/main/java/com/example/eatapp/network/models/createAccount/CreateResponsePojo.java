package com.example.eatapp.network.models.createAccount;


import com.google.gson.annotations.SerializedName;

public class CreateResponsePojo{

	@SerializedName("response")
	private Response response;

	@SerializedName("customer_id")
	private String customerId;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return
			"TestResponse{" +
			"response = '" + response + '\'' +
			",customer_id = '" + customerId + '\'' +
			"}";
		}
}