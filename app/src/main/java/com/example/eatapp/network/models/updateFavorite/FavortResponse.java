package com.example.eatapp.network.models.updateFavorite;


import com.google.gson.annotations.SerializedName;


public class FavortResponse{

	@SerializedName("response")
	private Response response;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"FavortResponse{" + 
			"response = '" + response + '\'' + 
			"}";
		}
}