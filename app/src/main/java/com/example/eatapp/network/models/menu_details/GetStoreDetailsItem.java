package com.example.eatapp.network.models.menu_details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GetStoreDetailsItem implements Parcelable {

	@SerializedName("image")
	private String image;

	@SerializedName("menu_item_id")
	private int menuItemId;

	@SerializedName("category_name_en")
	private String categoryNameEn;

	@SerializedName("status_id")
	private int statusId;

	@SerializedName("item_name_ar")
	private String itemNameAr;

	@SerializedName("category_name_ar")
	private String categoryNameAr;

	@SerializedName("item_name_en")
	private String itemNameEn;

	@SerializedName("price_item")
	private double  priceItem;

	@SerializedName("favorite")
	private String favorite;

	@SerializedName("menu_id")
	private String menuId;

	@SerializedName("offer_id")
	private int offerId;

	@SerializedName("item_description")
	private String itemDescription;

	protected GetStoreDetailsItem(Parcel in) {
		image = in.readString();
		menuItemId = in.readInt();
		categoryNameEn = in.readString();
		statusId = in.readInt();
		itemNameAr = in.readString();
		categoryNameAr = in.readString();
		itemNameEn = in.readString();
		priceItem = in.readDouble();
		favorite = in.readString();
		menuId = in.readString();
		offerId = in.readInt();
		itemDescription = in.readString();
	}

	public static final Creator<GetStoreDetailsItem> CREATOR = new Creator<GetStoreDetailsItem>() {
		@Override
		public GetStoreDetailsItem createFromParcel(Parcel in) {
			return new GetStoreDetailsItem(in);
		}

		@Override
		public GetStoreDetailsItem[] newArray(int size) {
			return new GetStoreDetailsItem[size];
		}
	};

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setMenuItemId(int menuItemId){
		this.menuItemId = menuItemId;
	}

	public int getMenuItemId(){
		return menuItemId;
	}

	public void setCategoryNameEn(String categoryNameEn){
		this.categoryNameEn = categoryNameEn;
	}

	public String getCategoryNameEn(){
		return categoryNameEn;
	}

	public void setStatusId(int statusId){
		this.statusId = statusId;
	}

	public int getStatusId(){
		return statusId;
	}

	public void setItemNameAr(String itemNameAr){
		this.itemNameAr = itemNameAr;
	}

	public String getItemNameAr(){
		return itemNameAr;
	}

	public void setCategoryNameAr(String categoryNameAr){
		this.categoryNameAr = categoryNameAr;
	}

	public String getCategoryNameAr(){
		return categoryNameAr;
	}

	public void setItemNameEn(String itemNameEn){
		this.itemNameEn = itemNameEn;
	}

	public String getItemNameEn(){
		return itemNameEn;
	}

	public void setPriceItem(double priceItem){
		this.priceItem = priceItem;
	}

	public double getPriceItem(){
		return priceItem;
	}

	public void setFavorite(String favorite){
		this.favorite = favorite;
	}

	public String getFavorite(){
		return favorite;
	}

	public void setMenuId(String menuId){
		this.menuId = menuId;
	}

	public String getMenuId(){
		return menuId;
	}

	public void setOfferId(int offerId){
		this.offerId = offerId;
	}

	public int getOfferId(){
		return offerId;
	}

	public void setItemDescription(String itemDescription){
		this.itemDescription = itemDescription;
	}

	public String getItemDescription(){
		return itemDescription;
	}

	@Override
 	public String toString(){
		return 
			"GetStoreDetailsItem{" + 
			"image = '" + image + '\'' +
			",menu_item_id = '" + menuItemId + '\'' +
			",category_name_en = '" + categoryNameEn + '\'' + 
			",status_id = '" + statusId + '\'' + 
			",item_name_ar = '" + itemNameAr + '\'' + 
			",category_name_ar = '" + categoryNameAr + '\'' + 
			",item_name_en = '" + itemNameEn + '\'' + 
			",price_item = '" + priceItem + '\'' + 
			",menu_id = '" + menuId + '\'' + 
			",offer_id = '" + offerId + '\'' + 
			",item_description = '" + itemDescription + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(image);
		dest.writeInt(menuItemId);
		dest.writeString(categoryNameEn);
		dest.writeInt(statusId);
		dest.writeString(itemNameAr);
		dest.writeString(categoryNameAr);
		dest.writeString(itemNameEn);
		dest.writeDouble(priceItem);
		dest.writeString(favorite);
		dest.writeString(menuId);
		dest.writeInt(offerId);
		dest.writeString(itemDescription);
	}
}