package com.example.eatapp.network.models.favort;


import com.google.gson.annotations.SerializedName;


public class GetMenuFavoriteItem{

	@SerializedName("image")
	private String image;

	@SerializedName("status_id")
	private int statusId;

	@SerializedName("item_name_en")
	private String itemNameEn;

	@SerializedName("item_description")
	private String itemDescription;

	@SerializedName("price_item")
	private String priceItem;

	@SerializedName("offer_id")
	private int offerId;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setStatusId(int statusId){
		this.statusId = statusId;
	}

	public int getStatusId(){
		return statusId;
	}

	public void setItemNameEn(String itemNameEn){
		this.itemNameEn = itemNameEn;
	}

	public String getItemNameEn(){
		return itemNameEn;
	}

	public void setItemDescription(String itemDescription){
		this.itemDescription = itemDescription;
	}

	public String getItemDescription(){
		return itemDescription;
	}

	public void setPriceItem(String priceItem){
		this.priceItem = priceItem;
	}

	public String getPriceItem(){
		return priceItem;
	}

	public void setOfferId(int offerId){
		this.offerId = offerId;
	}

	public int getOfferId(){
		return offerId;
	}

	@Override
 	public String toString(){
		return 
			"GetMenuFavoriteItem{" + 
			"image = '" + image + '\'' + 
			",status_id = '" + statusId + '\'' + 
			",item_name_en = '" + itemNameEn + '\'' + 
			",item_description = '" + itemDescription + '\'' + 
			",price_item = '" + priceItem + '\'' + 
			",offer_id = '" + offerId + '\'' + 
			"}";
		}
}