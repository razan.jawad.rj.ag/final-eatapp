package com.example.eatapp.network.models.menu;


import com.google.gson.annotations.SerializedName;

public class CompaniesTypeItem{

	@SerializedName("menu_item_id")
	private int menuItemId;

	@SerializedName("category_name_en")
	private String categoryNameEn;

	@SerializedName("status_id")
	private int statusId;

	@SerializedName("item_name_ar")
	private String itemNameAr;

	@SerializedName("category_name_ar")
	private String categoryNameAr;

	@SerializedName("item_name_en")
	private String itemNameEn;

	@SerializedName("price_item")
	private int priceItem;

	@SerializedName("menu_id")
	private String menuId;

	@SerializedName("offer_id")
	private int offerId;

	@SerializedName("item_description")
	private String itemDescription;

	public void setMenuItemId(int menuItemId){
		this.menuItemId = menuItemId;
	}

	public int getMenuItemId(){
		return menuItemId;
	}

	public void setCategoryNameEn(String categoryNameEn){
		this.categoryNameEn = categoryNameEn;
	}

	public String getCategoryNameEn(){
		return categoryNameEn;
	}

	public void setStatusId(int statusId){
		this.statusId = statusId;
	}

	public int getStatusId(){
		return statusId;
	}

	public void setItemNameAr(String itemNameAr){
		this.itemNameAr = itemNameAr;
	}

	public String getItemNameAr(){
		return itemNameAr;
	}

	public void setCategoryNameAr(String categoryNameAr){
		this.categoryNameAr = categoryNameAr;
	}

	public String getCategoryNameAr(){
		return categoryNameAr;
	}

	public void setItemNameEn(String itemNameEn){
		this.itemNameEn = itemNameEn;
	}

	public String getItemNameEn(){
		return itemNameEn;
	}

	public void setPriceItem(int priceItem){
		this.priceItem = priceItem;
	}

	public int getPriceItem(){
		return priceItem;
	}

	public void setMenuId(String menuId){
		this.menuId = menuId;
	}

	public String getMenuId(){
		return menuId;
	}

	public void setOfferId(int offerId){
		this.offerId = offerId;
	}

	public int getOfferId(){
		return offerId;
	}

	public void setItemDescription(String itemDescription){
		this.itemDescription = itemDescription;
	}

	public String getItemDescription(){
		return itemDescription;
	}

	@Override
	public String toString(){
		return
				"GetStoreDetailsItem{" +
						"menu_item_id = '" + menuItemId + '\'' +
						",category_name_en = '" + categoryNameEn + '\'' +
						",status_id = '" + statusId + '\'' +
						",item_name_ar = '" + itemNameAr + '\'' +
						",category_name_ar = '" + categoryNameAr + '\'' +
						",item_name_en = '" + itemNameEn + '\'' +
						",price_item = '" + priceItem + '\'' +
						",menu_id = '" + menuId + '\'' +
						",offer_id = '" + offerId + '\'' +
						",item_description = '" + itemDescription + '\'' +
						"}";
	}
}