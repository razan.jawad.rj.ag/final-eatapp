package com.example.eatapp.network.models.addOffer;


import com.google.gson.annotations.SerializedName;


public class AddOfferResponse{

	@SerializedName("response")
	private Response response;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"AddOfferResponse{" + 
			"response = '" + response + '\'' + 
			"}";
		}
}