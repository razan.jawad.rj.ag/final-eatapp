package com.example.eatapp.network.models.getOffer;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ReadOfferResponse{

	@SerializedName("response")
	private Response response;

	@SerializedName("getOffer")
	private List<GetOfferItem> getOffer;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setGetOffer(List<GetOfferItem> getOffer){
		this.getOffer = getOffer;
	}

	public List<GetOfferItem> getGetOffer(){
		return getOffer;
	}

	@Override
 	public String toString(){
		return 
			"ReadOfferResponse{" + 
			"response = '" + response + '\'' + 
			",getOffer = '" + getOffer + '\'' + 
			"}";
		}
}