package com.example.eatapp.network.models.updateFavorite;


import com.google.gson.annotations.SerializedName;


public class FavortRequest{

	@SerializedName("menu_item_id")
	private String menuItemId;

	@SerializedName("favorite")
	private String favorite;

	public void setMenuItemId(String menuItemId){
		this.menuItemId = menuItemId;
	}

	public String getMenuItemId(){
		return menuItemId;
	}

	public void setFavorite(String favorite){
		this.favorite = favorite;
	}

	public String getFavorite(){
		return favorite;
	}

	@Override
 	public String toString(){
		return 
			"FavortRequest{" + 
			"menu_item_id = '" + menuItemId + '\'' + 
			",favorite = '" + favorite + '\'' + 
			"}";
		}
}