package com.example.eatapp.network.models.read_tackaway_bill;


import com.google.gson.annotations.SerializedName;


public class ReadingBill{

	@SerializedName("cooking_time")
	private String cookingTime;

	@SerializedName("customer_id")
	private String customerId;

	public void setCookingTime(String cookingTime){
		this.cookingTime = cookingTime;
	}

	public String getCookingTime(){
		return cookingTime;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"ReadingBill{" + 
			"cooking_time = '" + cookingTime + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}