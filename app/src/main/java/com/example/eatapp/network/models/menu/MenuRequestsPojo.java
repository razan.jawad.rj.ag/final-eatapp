package com.example.eatapp.network.models.menu;

import com.google.gson.annotations.SerializedName;


public class MenuRequestsPojo {

	@SerializedName("menu_id")
	private String menuId;

	public void setMenuId(String menuId){
		this.menuId = menuId;
	}

	public String getMenuId(){
		return menuId;
	}

	@Override
	public String toString(){
		return
				"MenuRequestsPojoNEW{" +
						"menu_id = '" + menuId + '\'' +
						"}";
	}
}