package com.example.eatapp.network.models.managerReadOrderDetails;


import com.google.gson.annotations.SerializedName;


public class OrderDetailsRequst{

	@SerializedName("order_type_id")
	private String orderTypeId;

	@SerializedName("customer_id")
	private String customerId;

	public void setOrderTypeId(String orderTypeId){
		this.orderTypeId = orderTypeId;
	}

	public String getOrderTypeId(){
		return orderTypeId;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"OrderDetailsRequst{" + 
			"order_type_id = '" + orderTypeId + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}