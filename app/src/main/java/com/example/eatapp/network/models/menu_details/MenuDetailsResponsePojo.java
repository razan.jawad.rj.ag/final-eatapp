package com.example.eatapp.network.models.menu_details;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MenuDetailsResponsePojo{

	@SerializedName("response")
	private Response response;

	@SerializedName("getStore_Details")
	private List<GetStoreDetailsItem> getStoreDetails;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setGetStoreDetails(List<GetStoreDetailsItem> getStoreDetails){
		this.getStoreDetails = getStoreDetails;
	}

	public List<GetStoreDetailsItem> getGetStoreDetails(){
		return getStoreDetails;
	}

	@Override
	public String toString(){
		return
				"MenuResponseNEW{" +
						"response = '" + response + '\'' +
						",getStore_Details = '" + getStoreDetails + '\'' +
						"}";
	}
}