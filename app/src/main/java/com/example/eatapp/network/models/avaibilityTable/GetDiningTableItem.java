package com.example.eatapp.network.models.avaibilityTable;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.stream.Stream;


public class GetDiningTableItem implements Parcelable {

	@SerializedName("table_place")
	private String tablePlace;

	@SerializedName("chairs_count")
	private String chairsCount;

	@SerializedName("table_num")
	private String tableNum;

	protected GetDiningTableItem(Parcel in) {
		tablePlace = in.readString();
		chairsCount = in.readString();
		tableNum = in.readString();
	}

	public static final Creator<GetDiningTableItem> CREATOR = new Creator<GetDiningTableItem>() {
		@Override
		public GetDiningTableItem createFromParcel(Parcel in) {
			return new GetDiningTableItem(in);
		}

		@Override
		public GetDiningTableItem[] newArray(int size) {
			return new GetDiningTableItem[size];
		}
	};

	public void setTablePlace(String tablePlace){
		this.tablePlace = tablePlace;
	}

	public String getTablePlace(){
		return tablePlace;
	}

	public void setChairsCount(String chairsCount){
		this.chairsCount = chairsCount;
	}

	public String getChairsCount(){
		return chairsCount;
	}

	public void setTableNum(String tableNum){
		this.tableNum = tableNum;
	}

	public String getTableNum(){
		return tableNum;
	}

	@Override
 	public String toString(){
		return 
			"GetDiningTableItem{" + 
			"table_place = '" + tablePlace + '\'' + 
			",chairs_count = '" + chairsCount + '\'' + 
			",table_num = '" + tableNum + '\'' + 
			"}";
		}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(tablePlace);
		dest.writeString(chairsCount);
		dest.writeString(tableNum);
	}
}