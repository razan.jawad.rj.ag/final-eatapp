package com.example.eatapp.network.models.avaibilityTable;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class AvailabilityTableResponse{

	@SerializedName("getDiningTable")
	private List<GetDiningTableItem> getDiningTable;

	@SerializedName("response")
	private Response response;

	public void setGetDiningTable(List<GetDiningTableItem> getDiningTable){
		this.getDiningTable = getDiningTable;
	}

	public List<GetDiningTableItem> getGetDiningTable(){
		return getDiningTable;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"AvailabilityTableResponse{" + 
			"getDiningTable = '" + getDiningTable + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}