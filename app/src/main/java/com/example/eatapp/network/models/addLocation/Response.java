package com.example.eatapp.network.models.addLocation;


import com.google.gson.annotations.SerializedName;


public class Response{

	@SerializedName("response_code")
	private int responseCode;

	@SerializedName("message")
	private String message;

	public void setResponseCode(int responseCode){
		this.responseCode = responseCode;
	}

	public int getResponseCode(){
		return responseCode;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"response_code = '" + responseCode + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}