package com.example.eatapp.network.models.readCustomersOrders;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ReadCustomersOrdersResponse{

	@SerializedName("response")
	private Response response;

	@SerializedName("getCustomer_Orders")
	private List<GetCustomerOrdersItem> getCustomerOrders;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setGetCustomerOrders(List<GetCustomerOrdersItem> getCustomerOrders){
		this.getCustomerOrders = getCustomerOrders;
	}

	public List<GetCustomerOrdersItem> getGetCustomerOrders(){
		return getCustomerOrders;
	}

	@Override
 	public String toString(){
		return 
			"ReadCustomersOrdersResponse{" + 
			"response = '" + response + '\'' + 
			",getCustomer_Orders = '" + getCustomerOrders + '\'' + 
			"}";
		}
}