package com.example.eatapp.network.models.readCustomersOrders;


import com.google.gson.annotations.SerializedName;


public class GetCustomerOrdersItem{

	@SerializedName("customer_name_en")
	private String customerNameEn;

	@SerializedName("order_type_id")
	private String orderTypeId;

	@SerializedName("phone")
	private String phone;

	@SerializedName("email")
	private String email;

	public void setCustomerNameEn(String customerNameEn){
		this.customerNameEn = customerNameEn;
	}

	public String getCustomerNameEn(){
		return customerNameEn;
	}

	public void setOrderTypeId(String orderTypeId){
		this.orderTypeId = orderTypeId;
	}

	public String getOrderTypeId(){
		return orderTypeId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"GetCustomerOrdersItem{" + 
			"customer_name_en = '" + customerNameEn + '\'' + 
			",order_type_id = '" + orderTypeId + '\'' + 
			",phone = '" + phone + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}