package com.example.eatapp.network.models.read_tackaway_bill;


import com.google.gson.annotations.SerializedName;


public class ReadTackawayBillRequest{

	@SerializedName("customer_id")
	private String customerId;

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"ReadTackawayBillRequest{" + 
			"customer_id = '" + customerId + '\'' + 
			"}";
		}
}