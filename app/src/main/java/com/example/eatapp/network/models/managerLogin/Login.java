package com.example.eatapp.network.models.managerLogin;


import com.google.gson.annotations.SerializedName;


public class Login{

	@SerializedName("admin_user_id")
	private String adminUserId;

	public void setAdminUserId(String adminUserId){
		this.adminUserId = adminUserId;
	}

	public String getAdminUserId(){
		return adminUserId;
	}

	@Override
 	public String toString(){
		return 
			"Login{" + 
			"admin_user_id = '" + adminUserId + '\'' + 
			"}";
		}
}