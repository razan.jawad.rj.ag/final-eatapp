package com.example.eatapp.network.models.favort;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class FavortResponse{

	@SerializedName("getMenu_Favorite")
	private List<GetMenuFavoriteItem> getMenuFavorite;

	@SerializedName("response")
	private Response response;

	public void setGetMenuFavorite(List<GetMenuFavoriteItem> getMenuFavorite){
		this.getMenuFavorite = getMenuFavorite;
	}

	public List<GetMenuFavoriteItem> getGetMenuFavorite(){
		return getMenuFavorite;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"FavortResponse{" + 
			"getMenu_Favorite = '" + getMenuFavorite + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}