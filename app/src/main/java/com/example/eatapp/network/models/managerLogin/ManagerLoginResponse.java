package com.example.eatapp.network.models.managerLogin;


import com.google.gson.annotations.SerializedName;


public class ManagerLoginResponse{

	@SerializedName("response")
	private Response response;

	@SerializedName("login")
	private Login login;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setLogin(Login login){
		this.login = login;
	}

	public Login getLogin(){
		return login;
	}

	@Override
 	public String toString(){
		return 
			"ManagerLoginResponse{" + 
			"response = '" + response + '\'' + 
			",login = '" + login + '\'' + 
			"}";
		}
}