package com.example.eatapp.network.models.managerReadOrderDetails;


import com.google.gson.annotations.SerializedName;


public class ReadingOrder{

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("item_name_en")
	private String itemNameEn;

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setItemNameEn(String itemNameEn){
		this.itemNameEn = itemNameEn;
	}

	public String getItemNameEn(){
		return itemNameEn;
	}

	@Override
 	public String toString(){
		return 
			"ReadingOrder{" + 
			"quantity = '" + quantity + '\'' + 
			",item_name_en = '" + itemNameEn + '\'' + 
			"}";
		}
}