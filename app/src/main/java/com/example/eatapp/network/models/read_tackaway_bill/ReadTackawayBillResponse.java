package com.example.eatapp.network.models.read_tackaway_bill;

import com.google.gson.annotations.SerializedName;


public class ReadTackawayBillResponse{

	@SerializedName("Reading Bill")
	private ReadingBill readingBill;

	@SerializedName("response")
	private Response response;

	public void setReadingBill(ReadingBill readingBill){
		this.readingBill = readingBill;
	}

	public ReadingBill getReadingBill(){
		return readingBill;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"ReadTackawayBillResponse{" + 
			"reading Bill = '" + readingBill + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}