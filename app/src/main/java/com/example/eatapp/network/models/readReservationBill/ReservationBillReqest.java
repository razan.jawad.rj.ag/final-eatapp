package com.example.eatapp.network.models.readReservationBill;


import com.google.gson.annotations.SerializedName;


public class ReservationBillReqest{

	@SerializedName("customer_id")
	private String customerId;

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"ReservationBillReqest{" + 
			"customer_id = '" + customerId + '\'' + 
			"}";
		}
}