package com.example.eatapp.network.models.readDeliveryBill;


import com.google.gson.annotations.SerializedName;


public class ReadingBill{

	@SerializedName("final_price")
	private String finalPrice;

	@SerializedName("estimate_time")
	private String estimateTime;

	@SerializedName("price_delivery")
	private String priceDelivery;

	@SerializedName("discount")
	private String discount;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("price_item")
	private String priceItem;

	public void setFinalPrice(String finalPrice){
		this.finalPrice = finalPrice;
	}

	public String getFinalPrice(){
		return finalPrice;
	}

	public void setEstimateTime(String estimateTime){
		this.estimateTime = estimateTime;
	}

	public String getEstimateTime(){
		return estimateTime;
	}

	public void setPriceDelivery(String priceDelivery){
		this.priceDelivery = priceDelivery;
	}

	public String getPriceDelivery(){
		return priceDelivery;
	}

	public void setDiscount(String discount){
		this.discount = discount;
	}

	public String getDiscount(){
		return discount;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setPriceItem(String priceItem){
		this.priceItem = priceItem;
	}

	public String getPriceItem(){
		return priceItem;
	}

	@Override
 	public String toString(){
		return 
			"ReadingBill{" + 
			"final_price = '" + finalPrice + '\'' + 
			",estimate_time = '" + estimateTime + '\'' + 
			",price_delivery = '" + priceDelivery + '\'' + 
			",discount = '" + discount + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",price_item = '" + priceItem + '\'' + 
			"}";
		}
}