package com.example.eatapp.network.models.readDeliveryBill;


import com.google.gson.annotations.SerializedName;


public class ReadDeliveryBillRequest{

	@SerializedName("estimate_time")
	private String estimateTime;

	@SerializedName("customer_id")
	private String customerId;

	public void setEstimateTime(String estimateTime){
		this.estimateTime = estimateTime;
	}

	public String getEstimateTime(){
		return estimateTime;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"ReadDeliveryBillRequest{" + 
			"estimate_time = '" + estimateTime + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}