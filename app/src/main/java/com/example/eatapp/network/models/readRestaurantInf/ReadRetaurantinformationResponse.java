package com.example.eatapp.network.models.readRestaurantInf;


import com.google.gson.annotations.SerializedName;


public class ReadRetaurantinformationResponse{

	@SerializedName("response")
	private Response response;

	@SerializedName("Reading Customer")
	private ReadingCustomer readingCustomer;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setReadingCustomer(ReadingCustomer readingCustomer){
		this.readingCustomer = readingCustomer;
	}

	public ReadingCustomer getReadingCustomer(){
		return readingCustomer;
	}

	@Override
 	public String toString(){
		return 
			"ReadRetaurantinformationResponse{" + 
			"response = '" + response + '\'' + 
			",reading Customer = '" + readingCustomer + '\'' + 
			"}";
		}
}