package com.example.eatapp.network.models.menu_details;

import com.google.gson.annotations.SerializedName;

public class MenuDetailsRequest{

	@SerializedName("menu_id")
	private String menuId;

	public void setMenuId(String menuId){
		this.menuId = menuId;
	}

	public String getMenuId(){
		return menuId;
	}

	@Override
	public String toString(){
		return
				"Request{" +
						"menu_id = '" + menuId + '\'' +
						"}";
	}
}