package com.example.eatapp.network.models.updateOrder;


import com.google.gson.annotations.SerializedName;


public class UpdateOrderReqest{

	@SerializedName("menu_item_id")
	private String menuItemId;

	@SerializedName("final_price")
	private String finalPrice;

	@SerializedName("order_type_id")
	private String orderTypeId;

	@SerializedName("item_name_en")
	private String itemNameEn;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("order_id")
	private String orderId;

	public void setMenuItemId(String menuItemId){
		this.menuItemId = menuItemId;
	}

	public String getMenuItemId(){
		return menuItemId;
	}

	public void setFinalPrice(String finalPrice){
		this.finalPrice = finalPrice;
	}

	public String getFinalPrice(){
		return finalPrice;
	}

	public void setOrderTypeId(String orderTypeId){
		this.orderTypeId = orderTypeId;
	}

	public String getOrderTypeId(){
		return orderTypeId;
	}

	public void setItemNameEn(String itemNameEn){
		this.itemNameEn = itemNameEn;
	}

	public String getItemNameEn(){
		return itemNameEn;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	@Override
 	public String toString(){
		return 
			"UpdateOrderReqest{" + 
			"menu_item_id = '" + menuItemId + '\'' + 
			",final_price = '" + finalPrice + '\'' + 
			",order_type_id = '" + orderTypeId + '\'' + 
			",item_name_en = '" + itemNameEn + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",order_id = '" + orderId + '\'' + 
			"}";
		}
}