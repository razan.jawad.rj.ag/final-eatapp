package com.example.eatapp.network.models.getReservationDateTime;


import com.google.gson.annotations.SerializedName;


public class GetReservationDateTimeRequst{
	@SerializedName("customerId")
	private String customerId;

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}
	@Override
 	public String toString(){
		return 
			"GetReservationDateTimeRequst{" + 
			"}";
		}
}