package com.example.eatapp.network.models.addLocation;

import com.google.gson.annotations.SerializedName;


public class AddLocationRequst{

	@SerializedName("width")
	private String width;

	@SerializedName("hight")
	private String hight;

	@SerializedName("customer_id")
	private String customerId;

	public void setWidth(String width){
		this.width = width;
	}

	public String getWidth(){
		return width;
	}

	public void setHight(String hight){
		this.hight = hight;
	}

	public String getHight(){
		return hight;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"AddLocationRequst{" + 
			"width = '" + width + '\'' + 
			",hight = '" + hight + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}