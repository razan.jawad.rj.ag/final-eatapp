package com.example.eatapp.network.models.updateOrder;


import com.google.gson.annotations.SerializedName;

public class UpdateOrderResponse{

	@SerializedName("response")
	private Response response;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"UpdateOrderResponse{" + 
			"response = '" + response + '\'' + 
			"}";
		}
}