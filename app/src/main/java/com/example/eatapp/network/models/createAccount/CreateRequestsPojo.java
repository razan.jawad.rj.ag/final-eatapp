package com.example.eatapp.network.models.createAccount;


import com.google.gson.annotations.SerializedName;


public class CreateRequestsPojo {

	@SerializedName("customer_name_en")
	private String customerNameEn;

	@SerializedName("password")
	private String password;

	@SerializedName("phone")
	private String phone;

	@SerializedName("user_name")
	private String userName;

	@SerializedName("city_name_en")
	private String cityNameEn;

	@SerializedName("email")
	private String email;

	public void setCustomerNameEn(String customerNameEn){
		this.customerNameEn = customerNameEn;
	}

	public String getCustomerNameEn(){
		return customerNameEn;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setCityNameEn(String cityNameEn){
		this.cityNameEn = cityNameEn;
	}

	public String getCityNameEn(){
		return cityNameEn;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"CreateRequestsPojo{" +
			"customer_name_en = '" + customerNameEn + '\'' + 
			",password = '" + password + '\'' + 
			",phone = '" + phone + '\'' + 
			",user_name = '" + userName + '\'' + 
			",city_name_en = '" + cityNameEn + '\'' +
			",email = '" + email + '\'' + 
			"}";
		}
}