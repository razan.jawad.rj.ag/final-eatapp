package com.example.eatapp.network.models.addReservationCustomer;


import com.google.gson.annotations.SerializedName;


public class AddResevationCustomerRequest{

	@SerializedName("reservationEndDate")
	private String reservationEndDate;

	@SerializedName("customerId")
	private String customerId;

	@SerializedName("reservationStartDate")
	private String reservationStartDate;

	@SerializedName("diningTableId")
	private String diningTableId;

	@SerializedName("personNum")
	private String personNum;

	public void setReservationEndDate(String reservationEndDate){
		this.reservationEndDate = reservationEndDate;
	}

	public String getReservationEndDate(){
		return reservationEndDate;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	public void setReservationStartDate(String reservationStartDate){
		this.reservationStartDate = reservationStartDate;
	}

	public String getReservationStartDate(){
		return reservationStartDate;
	}

	public void setDiningTableId(String diningTableId){
		this.diningTableId = diningTableId;
	}

	public String getDiningTableId(){
		return diningTableId;
	}

	public void setPersonNum(String personNum){
		this.personNum = personNum;
	}

	public String getPersonNum(){
		return personNum;
	}

	@Override
 	public String toString(){
		return 
			"AddResevationCustomerRequest{" + 
			"reservationEndDate = '" + reservationEndDate + '\'' + 
			",customerId = '" + customerId + '\'' + 
			",reservationStartDate = '" + reservationStartDate + '\'' + 
			",diningTableId = '" + diningTableId + '\'' + 
			",personNum = '" + personNum + '\'' + 
			"}";
		}
}