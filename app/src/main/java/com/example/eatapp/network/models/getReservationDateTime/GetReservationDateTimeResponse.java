package com.example.eatapp.network.models.getReservationDateTime;


import com.google.gson.annotations.SerializedName;


public class GetReservationDateTimeResponse{

	@SerializedName("Reading reservation customer")
	private ReadingReservationCustomer readingReservationCustomer;

	@SerializedName("response")
	private Response response;

	public void setReadingReservationCustomer(ReadingReservationCustomer readingReservationCustomer){
		this.readingReservationCustomer = readingReservationCustomer;
	}

	public ReadingReservationCustomer getReadingReservationCustomer(){
		return readingReservationCustomer;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"GetReservationDateTimeResponse{" + 
			"reading reservation customer = '" + readingReservationCustomer + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}