package com.example.eatapp.network.models.ReadManagerDilevaryPrice;


import com.google.gson.annotations.SerializedName;


public class DeliveryPriceAndPhoneRequst {

	@SerializedName("order_type_id")
	private String orderTypeId;

	@SerializedName("customer_id")
	private String customerId;

	public void setOrderTypeId(String orderTypeId){
		this.orderTypeId = orderTypeId;
	}

	public String getOrderTypeId(){
		return orderTypeId;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"DeliveryPriceAndPhoneRequst{" +
			"order_type_id = '" + orderTypeId + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}