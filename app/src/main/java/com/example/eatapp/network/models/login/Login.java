package com.example.eatapp.network.models.login;


import com.google.gson.annotations.SerializedName;

public class Login{

	@SerializedName("user_name")
	private String userName;

	@SerializedName("customer_id")
	private String customerId;

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"Login{" + 
			"user_name = '" + userName + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}