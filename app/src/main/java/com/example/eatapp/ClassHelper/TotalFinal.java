package com.example.eatapp.ClassHelper;

import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.List;

public class TotalFinal {

   public static double numberDiscount;


    public static double totalPrice(List<MenuItemModel> cartList) {
        double sum = 0, totaltax;
        for (int i = 0; i < cartList.size(); i++) {
            sum += cartList.get(i).getQuantity() * cartList.get(i).getPrice();
        }
        return sum;
    }

    public static double sumDiscountPrice(double sum) {
        final double s_discount = numberDiscount;
        double discount = 0;
        discount = s_discount * sum;
        return sum -= discount;

    }

    public static double sumDelivaryPrice(double sum) {
        final double s_Delivary = 3.00;
        return sum += s_Delivary;

    }


}
