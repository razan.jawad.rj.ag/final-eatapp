package com.example.eatapp.ClassHelper;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ConverDoubleToString {
    private  static NumberFormat formatter = new DecimalFormat("#0.00");


    public  static String ConDoubleString(double number){
        return String.valueOf(formatter.format(number));
    }
}
