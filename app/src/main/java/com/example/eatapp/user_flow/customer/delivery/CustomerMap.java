package com.example.eatapp.user_flow.customer.delivery;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.googleApi.IgoogleApiEndPoint;
import com.example.eatapp.network.models.addLocation.AddLocationRequst;
import com.example.eatapp.network.models.addLocation.AddLocationResponse;
import com.example.eatapp.user_flow.customer.TakeAwayActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerMap extends FragmentActivity implements OnMapReadyCallback,
        LocationListener, GoogleApiClient.ConnectionCallbacks
        , GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;

    private static int UPDATE_INTERVAL = 1000;
    private static int FATEST_INTERVAL = 5000;
    private static int DISPLACEEMENT = 10;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 100;
    private final static int LOCATION_PERMISSION_REQUEST = 100;
    //private final static String ADDRESS = "962 Amman ";

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private Location mLastLocation;
    private double latitude, longitude;
    private static String LAT = "Lat";
    private static String LNG= "Lng";
    //will save in database
    double x, y;
    private String customerId;
    Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

    private IgoogleApiEndPoint mService;

    private Button bnSelectLocation;
    private ImageView imgLocation;
    private static String PAGE_ID_KEY = "page_id_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_map);

        bnSelectLocation = findViewById(R.id.bn_select_your_location);
        imgLocation = findViewById(R.id.img_add_location);
        customerId = SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this);

        //to grt palce form google
        mService = ShopingInformation.getGeoIgoogleApiEndPoint();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            if (checkPlaysServices())
                buildGoogleApiClint();
            createLocationRequst();

        }
        displayLocation();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        bnSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                        //Toast.makeText(CustomerMap.this, (x + " " + y), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(CustomerMap.this, TakeAwayActivity.class);
                        intent.putExtra(PAGE_ID_KEY, 2);
                        intent.putExtra(LAT,x);
                        intent.putExtra(LNG,y);
                        startActivity(intent);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlaysServices()) {
                        buildGoogleApiClint();
                        createLocationRequst();
                        displayLocation();
                    }

                }
                break;


        }
    }

    private void createLocationRequst() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEEMENT);

    }

    private boolean checkPlaysServices() {
        int resultcode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultcode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultcode)) {
                GooglePlayServicesUtil.getErrorDialog(resultcode, this, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(this, "this device is not support", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    protected synchronized void buildGoogleApiClint() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            Task<Location> task = fusedLocationProviderClient.getLastLocation();
            task.addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mLastLocation = location;
                    if (mLastLocation != null) {
                        latitude = mLastLocation.getLatitude();
                        longitude = mLastLocation.getLongitude();

                        LatLng yourLocation = new LatLng(latitude, longitude);
                        //move camera
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(yourLocation) // Sets the center of the map
                                .tilt(20) // Sets the tilt of the camera to 20 degrees
                                .zoom(17) // Sets the zoom
                                .bearing(90) // Sets the orientation of the camera to east
                                .build();


                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        // mMap.animateCamera(CameraUpdateFactory.newLatLng(yourLocation));
                        //  mMap.animateCamera(CameraUpdateFactory.zoomTo(10.0f));
//                        double numberDiscount = mMap.getCameraPosition().target.latitude;
//                        double y = mMap.getCameraPosition().target.longitude;

                    } else {
                        Toast.makeText(CustomerMap.this, "could not get the Location", Toast.LENGTH_SHORT).show();
                    }

                    mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                        @Override
                        public void onCameraIdle() {
                            LatLng chang = mMap.getCameraPosition().target;

                            x = mMap.getCameraPosition().target.latitude;
                            y = mMap.getCameraPosition().target.longitude;

                        }
                    });

                }
            });
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        displayLocation();
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                //   Toast.makeText(CustomerMap.this, ("AndroidClarified" + location.getLatitude() + " " + location.getLongitude()), Toast.LENGTH_SHORT).show();
            }
        });


//old code 2017
        // LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (com.google.android.gms.location.LocationListener) this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    private void changedLocation() {
        // Setting a click event handler for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                // Creating a marker
                // MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                //  markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
                //markerOptions.title(latLng.latitude + " : " + latLng.longitude);


                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_add_location);
                bitmap = ShopingInformation.scaleBitmap(bitmap, 70, 70);

                MarkerOptions marker = new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromBitmap(bitmap))
                        .title("ordet of phone")
                        .position(latLng);

                mMap.addMarker(marker);

                // Clears the previously touched position
                mMap.clear();

                // Animating to the touched position
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                // Placing a marker on the touched position
                mMap.addMarker(marker);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlaysServices();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.FailureApi)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }
}
