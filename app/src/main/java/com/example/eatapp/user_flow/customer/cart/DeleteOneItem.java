package com.example.eatapp.user_flow.customer.cart;

import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

public interface DeleteOneItem {
    public void onClickOneItem(MenuItemModel menuItemModel);

}
