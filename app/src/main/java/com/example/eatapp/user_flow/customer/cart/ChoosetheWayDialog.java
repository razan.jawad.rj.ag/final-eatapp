package com.example.eatapp.user_flow.customer.cart;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.TakeAwayActivity;
import com.example.eatapp.user_flow.customer.bookYourTable.BookYourTableActivity;
import com.example.eatapp.user_flow.customer.delivery.DeliveryActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChoosetheWayDialog extends DialogFragment {
    private Button bnTakeAway, bnDelivary, bnBookYourTable;
    private static String PAGE_ID_KEY = "page_id_key";
    private TypeOrder typeOrder;

    public void setTypeOrder(TypeOrder typeOrder) {
        this.typeOrder = typeOrder;
    }

    public ChoosetheWayDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_choosethe_way_dialog, container, false);

        setUp(v);

        return v;
    }

    private void setUp(View v) {
        bnTakeAway = v.findViewById(R.id.bn_take_away);
        bnBookYourTable = v.findViewById(R.id.bn_book_your_table_and_order);
        bnDelivary = v.findViewById(R.id.bn_delivery);

        bnTakeAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeOrder.onClickOrder(1);
                dismiss();

                //will
//                Intent intent = new Intent(getContext(), TakeAwayActivity.class);
//                intent.putExtra(PAGE_ID_KEY, 0);
//                startActivity(intent);
//                dismiss();
            }
        });

        bnBookYourTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeOrder.onClickOrder(2);
                dismiss();

            }
        });

        bnDelivary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeOrder.onClickOrder(3);
                dismiss();

            }
        });
    }

}
