package com.example.eatapp.user_flow.manager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.ConvertBitmapToByte;
import com.example.eatapp.R;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickCancel;
import com.vansuita.pickimage.listeners.IPickResult;

public class AddCatogryToMenu extends AppCompatActivity {
    private ImageView imageViewCategory;
private String enCodedImage="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_catogry_to_menu);
        imageViewCategory=findViewById(R.id.imageView_category_add);


    }

    public void showimage(View view) {
        PickImageDialog.build(new PickSetup()).setOnPickResult(new IPickResult() {
            @Override
            public void onPickResult(PickResult pickResult) {
                imageViewCategory.setImageBitmap(pickResult.getBitmap());
                enCodedImage= ConvertBitmapToByte.convertToString(pickResult.getBitmap());

                //api
                //model.getImage(encodImagw);
            }
        }).setOnPickCancel(new IPickCancel() {
            @Override
            public void onCancelClick() {
                Toast.makeText(AddCatogryToMenu.this, "you cancel", Toast.LENGTH_SHORT).show();
            }
        }).show(getSupportFragmentManager());
    }
}
