package com.example.eatapp.user_flow.customer.menu;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.menu_details.GetStoreDetailsItem;
import com.example.eatapp.network.models.updateFavorite.FavortRequest;
import com.example.eatapp.network.models.updateFavorite.FavortResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuRecyclerAdapter extends RecyclerView.Adapter<MenuRecyclerAdapter.ViewItem> {

    private List<GetStoreDetailsItem> items;

    private Context context;
    private boolean isCheckrd = false;
    private String stringFavort;

    public MenuRecyclerAdapter(ArrayList<GetStoreDetailsItem> item, Context context) {
        this.items = item;
        this.context = context;

    }

    //onCreateViewHolder used to HAndle on Clicks
    @Override
    public ViewItem onCreateViewHolder(final ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_main_courses, parent, false);

        return new ViewItem(itemView);
    }


    //to fill each item with data from the array depending on position
    @Override
    public void onBindViewHolder(final ViewItem holder, final int position) {
        GetStoreDetailsItem currentItem = items.get(position);

        holder.name.setText(currentItem.getItemNameEn());
        holder.tnNumberPrice.setText(String.valueOf(currentItem.getPriceItem()) + " " + "JD");
        holder.information.setText(currentItem.getItemDescription());
        Glide.with(context).load("http://40.76.37.131:8080/rest/rest" + items.get(position).getImage()).into(holder.image);

        // http://40.76.37.131:8080/rest/restmenuItem_Kumpir(5).jpg
        //holder.image.setImageIcon(items.get(position).getPosterPath());
        // Log.d("movie_id",items.get(position).getId().toString());
        //https://image.tmdb.org/t/p/w500/kqjL17yufvn9OVLyXYpvtyrFfak.jpg
        //  Picasso.get().load("https://image.tmdb.org/t/p/w500"+items.get(position).getPosterPath()).into(holder.image);
        stringFavort = currentItem.getFavorite();
        if(stringFavort.equals("1")){
            holder.heart.setImageResource(R.drawable.ic_heart);
        }else
        {
            holder.heart.setImageResource(R.drawable.ic_favorite);
        }

    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;

        return items.size();
    }

    // to put custom dialog when prss at buttom
    private void showOrderDialog(GetStoreDetailsItem menuItemModel) {


        //create dialog                                 //fill with data
        NewOrderDialog newOrderDialog = NewOrderDialog.newInstance(menuItemModel.getPriceItem(), menuItemModel.getItemNameEn(),
                menuItemModel.getImage(),menuItemModel.getMenuItemId());

        //show dielog
        //1 coz we are in adapter to will do
        FragmentManager fm = ((AppCompatActivity) context).getSupportFragmentManager();
        //2 show dielog
        newOrderDialog.show(fm, "NewOrdwr_Fragment");
    }


    //The View Item part responsible for connecting the row.xml with
    // each item in the RecyclerView
    //make declare and initalize
    public class ViewItem extends RecyclerView.ViewHolder {

        //Declare
        private TextView name, information, tvPriceMenu, tnNumberPrice;
        private ImageView image;
        private ImageButton cart;
        private ImageButtonHart heart;

        //initialize
        public ViewItem(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.tv_img_menu);
            name = itemView.findViewById(R.id.tv_name);
            information = itemView.findViewById(R.id.tv_info);
            heart = itemView.findViewById(R.id.bn_heart);
            cart = itemView.findViewById(R.id.bn_cart);
            tvPriceMenu = itemView.findViewById(R.id.tv_price_minu);
            tnNumberPrice = itemView.findViewById(R.id.tv_number_price_minu);


            cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    showOrderDialog(items.get(getAdapterPosition()));
                }
            });


            heart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isCheckrd) {
                        heart.setImageResource(R.drawable.ic_heart);
                        isCheckrd = false;
                        favortApiConnected(items.get(getAdapterPosition()).getMenuItemId(), items.get(getAdapterPosition()).getFavorite());
                    } else {
                        isCheckrd = true;
                        heart.setImageResource(R.drawable.ic_favorite);
                        favortApiConnected(items.get(getAdapterPosition()).getMenuItemId(), items.get(getAdapterPosition()).getFavorite());
                    }
                }
            });
        }
    }

    private void favortApiConnected(int id, String fav) {

        Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

        final FavortRequest favortRequest = new FavortRequest();

        if (fav.equals("1")) {
            favortRequest.setFavorite("0");
        } else {
            favortRequest.setFavorite("1");
        }

        favortRequest.setMenuItemId(String.valueOf(id));

        Call<FavortResponse> call = apiService.getFavort(favortRequest);

        call.enqueue(new Callback<FavortResponse>() {
            @Override
            public void onResponse(Call<FavortResponse> call, Response<FavortResponse> response) {


                if (response.body().getResponse().getResponseCode() == 0) {
                    if (stringFavort.equals("0")) {
                        stringFavort = "1";
                        isCheckrd = true;
                    } else {
                        stringFavort = "1";
                        isCheckrd = false;
                    }
                }
            }

            @Override
            public void onFailure(Call<FavortResponse> call, Throwable t) {

            }
        });
    }

}
