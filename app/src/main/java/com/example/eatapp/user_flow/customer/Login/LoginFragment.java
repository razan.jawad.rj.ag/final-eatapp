package com.example.eatapp.user_flow.customer.Login;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.login.LoginRequestsPojo;
import com.example.eatapp.network.models.login.LoginResponsePojo;
import com.example.eatapp.network.models.managerLogin.ManagerLoginRequst;
import com.example.eatapp.network.models.managerLogin.ManagerLoginResponse;
import com.example.eatapp.user_flow.customer.CustomerActivity;
import com.example.eatapp.user_flow.manager.ManagerActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private Button bn_login;
    private EditText userName, password;
    private LocalDatabase dataBase;
    private TextView signUp, notRegitered;
    // private List<LoginDataModel> informationArrayList;
    private String name, pass;
    Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        setupViews(v);

        return v;

    }

    private void setupViews(View v) {
        bn_login = v.findViewById(R.id.bn_login);
        userName = v.findViewById(R.id.ed_enter_user_name);
        password = v.findViewById(R.id.ed_enter_your_password);
        notRegitered = v.findViewById(R.id.tv_not_registered);
        signUp = v.findViewById(R.id.tv_singup);


        dataBase = LocalDatabase.getAppDatabase(getContext());
        final LoginDataModel loginDataModel = new LoginDataModel();


        bn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectionApi();

            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.luanhActivity(getContext(), 1);

            }
        });
    }

    private void connectionApi() {
        //conected to servier
        if (IsInputValid()) {

            String nameManager = String.valueOf(userName.getText());
            String passManager = String.valueOf(password.getText());
            if (nameManager.equalsIgnoreCase("WArd") && passManager.equalsIgnoreCase("w123")) {
                // Toast.makeText(getContext(), "manager", Toast.LENGTH_SHORT).show();
                ManagerLoginRequst managerLoginRequst = new ManagerLoginRequst();
                managerLoginRequst.setUsername(nameManager);
                managerLoginRequst.setPassword(passManager);

                Call<ManagerLoginResponse> managerLoginCall = apiService.managerLogin(managerLoginRequst);
                managerLoginCall.enqueue(new Callback<ManagerLoginResponse>() {
                    @Override
                    public void onResponse(Call<ManagerLoginResponse> call, Response<ManagerLoginResponse> response) {

                        if (!response.body().getLogin().getAdminUserId().equals("-1")) {
                            Intent intent = new Intent(getContext(), ManagerActivity.class);
                            boolean manager = true;
                            SharedPreferencesOfMe.setISManagerDefaults(SharedPreferencesOfMe.IS_MANAGER, manager, getContext());
                            startActivity(intent);
                        }


                    }

                    @Override
                    public void onFailure(Call<ManagerLoginResponse> call, Throwable t) {
                        showDailogFailure();
                    }
                });


            } else {


                LoginRequestsPojo loginRequestsPojo = new LoginRequestsPojo();
                loginRequestsPojo.setUserName(userName.getText().toString());
                loginRequestsPojo.setPassword(password.getText().toString());

                Call<LoginResponsePojo> call = apiService.login(loginRequestsPojo);
                call.enqueue(new Callback<LoginResponsePojo>() {
                    @Override
                    public void onResponse(Call<LoginResponsePojo> call, Response<LoginResponsePojo> response) {

                        if (!response.body().getLogin().getCustomerId().equals("-1")) {
                            Intent intent = new Intent(getContext(), CustomerActivity.class);

                            //to make id sharing SharedPreferences
                            String id = response.body().getLogin().getCustomerId();
                            boolean loged = true;
                            SharedPreferencesOfMe.setCustomerIDDefaults(SharedPreferencesOfMe.CUSTOMET_ID, id, getContext());
                            SharedPreferencesOfMe.setISLogedDefaults(SharedPreferencesOfMe.IS_LOGED, loged, getContext());


                            //end SharedPreferences

                            startActivity(intent);
                        } else {
                            showDailoglogin();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponsePojo> call, Throwable t) {
                        showDailoglogin();
                    }
                });

            }
        }
    }


    //if the edit text empty
    private boolean IsInputValid() {
        name = userName.getText().toString();
        pass = password.getText().toString();

        if (TextUtils.isEmpty(name) && TextUtils.isEmpty(pass)) {

            userName.setError(getContext().getString(R.string.your_name_or_password_is_empty));
            password.setError(getContext().getString(R.string.your_password_is_empty));
            return false;
        } else if (TextUtils.isEmpty(name)) {
            userName.setError(getContext().getString(R.string.your_name_is_empty));
            return false;
        } else if (TextUtils.isEmpty(pass)) {
            password.setError(getContext().getString(R.string.your_password_is_empty));
            return false;
        } else
            return true;
    }

    //deffult dailog
    private void showDailoglogin() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.your_name_or_password_not_correct)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }

    //check at internet
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);


        return cm.getActiveNetworkInfo() != null;
    }


    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.FailureApi)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }




}


//conected to local database
          /*  GetInformationAsyncTask getInformationAsyncTask = new GetInformationAsyncTask();
            informationArrayList = null;
            try {
                informationArrayList = (ArrayList<LoginDataModel>) getInformationAsyncTask.execute().get();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //for loop to get full name and password from local datatbae and chaeck if he regstratoin and if he manager

            if (informationArrayList != null) {
                if (informationArrayList.size() == 0) {

                    showDailoglogin();
                }
                else {
                    for (int i = 0; i < informationArrayList.size(); i++) {
                        String namefromlocaldata = informationArrayList.get(i).getFullName();
                        String passfromlocaldata = informationArrayList.get(i).getPassword();
                        if (namefromlocaldata.equalsIgnoreCase(name)
                                && passfromlocaldata.equals(pass))

                            if (informationArrayList.get(i).isMangar() == true) {
                                Intent intent = new Intent(getContext(), ManagerActivity.class);
                                startActivity(intent);
                                break;
                            } else {
                                Intent intent = new Intent(getContext(), CustomerActivity.class);
                                startActivity(intent);
                                break;
                            }

                        else {

                            showDailoglogin();
                        }
                    }
                }
            }
            */
//                    else {
//
//                        showDailoglogin();
//                    }

//private class GetInformationAsyncTask extends AsyncTask<Void, Void, List<LoginDataModel>> {
//    @Override
//    protected List<LoginDataModel> doInBackground(Void... voids) {
//        return getInformation();
//    }
//}
//
//    private List<LoginDataModel> getInformation() {
//
//        return dataBase.daoUser().getAll();
//
//    }