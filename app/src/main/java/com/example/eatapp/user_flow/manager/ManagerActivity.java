package com.example.eatapp.user_flow.manager;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.CustomerActivity;
import com.example.eatapp.user_flow.manager.informationCustomer.InformationCustomerActivity;

public class ManagerActivity extends AppCompatActivity {

    private Button bnTable, bnTableFood, bnDelivery, bnTakeAway, bnAddDiscount, bnAddCategorymenu,bnlogout;
    private static String PAGE_ID_KEY = "page_id_key";

    // private ArrayList<LoginDataModel> infoCustomerArray = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);

        setup();


    }

    private void setup() {
        bnTableFood = findViewById(R.id.bn_table_and_food_manager);
        bnTable = findViewById(R.id.bn_table_manager);
        bnDelivery = findViewById(R.id.bn_delivery_manager);
        bnTakeAway = findViewById(R.id.bn_take_away_manager);
        bnAddDiscount = findViewById(R.id.bn_add_discount_manager);
        bnAddCategorymenu = findViewById(R.id.bn_add_category_manager);
        bnlogout=findViewById(R.id.bn_logotymanager);


        bnTakeAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerActivity.this, InformationCustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 1);
                startActivity(intent);
            }
        });


        bnTableFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerActivity.this, InformationCustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 2);
                startActivity(intent);
            }
        });
        bnTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerActivity.this, InformationCustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 3);
                startActivity(intent);
            }
        });
        bnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManagerActivity.this, InformationCustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 4);
                startActivity(intent);
            }
        });


        bnAddDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDiscountDialog();
            }
        });

        bnAddCategorymenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ManagerActivity.this, AddCatogryToMenu.class));
            }
        });


   bnlogout.setOnClickListener(new View.OnClickListener() {
       @Override
       public void onClick(View v) {
           Intent intent = new Intent(ManagerActivity.this, CustomerActivity.class);
           SharedPreferencesOfMe.setISManagerDefaults(SharedPreferencesOfMe.IS_MANAGER, false, ManagerActivity.this);
           startActivity(intent);
       }
   });

    }

    private void showDiscountDialog() {
        //create dialog
        AddDiscountDialog addDiscountDialog = new AddDiscountDialog();

        //show dielog
        //1 coz we are in adapter to will do
        FragmentManager fm = this.getSupportFragmentManager();

        //2 show dielog
        addDiscountDialog.show(fm, "NewOrdwr_Fragment");
    }

    //to BackPressed  go Out the app
    @Override
    public void onBackPressed() {

        this.finishAffinity();


    }


    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.FailureApi)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


}


