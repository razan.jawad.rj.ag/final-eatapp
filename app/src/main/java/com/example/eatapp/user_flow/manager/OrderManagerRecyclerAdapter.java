package com.example.eatapp.user_flow.manager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;

public class OrderManagerRecyclerAdapter extends RecyclerView.Adapter<OrderManagerRecyclerAdapter.ViewItem> {
//1
    private ArrayList<MenuItemModel> items;

//type order =1

//4
    public OrderManagerRecyclerAdapter(ArrayList<MenuItemModel> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_order_customer, viewGroup, false);


        return new OrderManagerRecyclerAdapter.ViewItem(itemView);
    }

    @Override
    public void onBindViewHolder(final OrderManagerRecyclerAdapter.ViewItem holder, final int position) {
     //2
        MenuItemModel currentItem = items.get(position);
//3
        holder.tvNameFood.setText(currentItem.getName());
        holder.tvQuantity.setText(String.valueOf(currentItem.getQuantity()));


    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;

        return items.size();
    }

    public class ViewItem extends RecyclerView.ViewHolder {

        //Declare
        TextView tvNameFood, tvQuantity;

        //initialize
        public ViewItem(View itemView) {
            super(itemView);
            tvNameFood = itemView.findViewById(R.id.tv_name_food_customer);
            tvQuantity = itemView.findViewById(R.id.tv_quantity_customer);


        }
    }
    //2 to get data from room

}
