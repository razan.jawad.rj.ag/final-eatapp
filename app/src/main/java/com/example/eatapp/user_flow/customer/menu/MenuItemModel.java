package com.example.eatapp.user_flow.customer.menu;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

@Entity(tableName = "Menu")

public class MenuItemModel implements Parcelable {
    @PrimaryKey
    @NonNull
    private String name;
    private double price;
    private String image;
    private int quantity;
    private int id_item;


    public MenuItemModel() {

    }
    public MenuItemModel(String name, double price, String image, int quantity ,int id){
        this.name = name;
        this.price = price;
        this.image = image;
        this.quantity = quantity;
        this.id_item=id;

    }
    public MenuItemModel(String name, double price,  int quantity ){
        this.name = name;
        this.price = price;
        this.quantity = quantity;

    }
    protected MenuItemModel(Parcel in) {
        name = in.readString();
        price = in.readDouble();
        image = in.readString();
        quantity = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeString(image);
        dest.writeInt(quantity);
        dest.writeInt(id_item);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MenuItemModel> CREATOR = new Creator<MenuItemModel>() {
        @Override
        public MenuItemModel createFromParcel(Parcel in) {
            return new MenuItemModel(in);
        }

        @Override
        public MenuItemModel[] newArray(int size) {
            return new MenuItemModel[size];
        }
    };

    public int getQuantity() {return quantity; }

    public void setQuantity(int quantity) { this.quantity = quantity; }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId_item() {
        return id_item;
    }

    public void setId_item(int id_item) {
        this.id_item = id_item;
    }
}
