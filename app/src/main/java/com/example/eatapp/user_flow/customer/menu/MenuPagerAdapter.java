package com.example.eatapp.user_flow.customer.menu;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.eatapp.R;
import com.example.eatapp.network.models.menu_details.GetStoreDetailsItem;

import java.util.ArrayList;

public class MenuPagerAdapter extends FragmentPagerAdapter {
    // 1 to chang name  of tab from english to arabic
    private Context context;
    //1 to Use one adapter for multiple arrays
    private ArrayList<GetStoreDetailsItem> appetisers;
    private ArrayList<GetStoreDetailsItem> mainCourses;
    private ArrayList<GetStoreDetailsItem> desserts;
    private ArrayList<GetStoreDetailsItem> beverages;


    public MenuPagerAdapter(
            FragmentManager fm,
            //2 to chang name  of tab from english to arabic
            Context context,
            ArrayList<GetStoreDetailsItem> appetisers,
            ArrayList<GetStoreDetailsItem> mainCourses,
            ArrayList<GetStoreDetailsItem> desserts,
            ArrayList<GetStoreDetailsItem> beverages) {

        super(fm);
        // 3 to chang name  of tab from english to arabic
        this.context = context;
        //2 to Use one adapter for multiple arrays
        this.context = context;
        this.appetisers = appetisers;
        this.mainCourses = mainCourses;
        this.desserts = desserts;
        this.beverages = beverages;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                //3 to Use one adapter for multiple arrays
                // NameOfFragmant nameOfFragment =NameOfFragmant.newInstance(name the list);
                return MenuListFragment.newInstance(appetisers);

            case 1:
                return MenuListFragment.newInstance(mainCourses);

            case 2:
                return MenuListFragment.newInstance(desserts);

            case 3:
                return MenuListFragment.newInstance(beverages);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                // 4 to chang name  of tab from english to arabic
                return context.getString(R.string.main_courses);

            case 1:
                return context.getString(R.string.appetisers);

            case 2:
                return context.getString(R.string.desserts);

            case 3:
                return context.getString(R.string.beverages);

        }
        return null;
    }
    // 4 donot forget chang  MenuPagerAdapter adapterFragmentViewPagerOfLogin=
    //                new MenuPagerAdapter(getSupportFragmentManager(),this);
    // in main
}
