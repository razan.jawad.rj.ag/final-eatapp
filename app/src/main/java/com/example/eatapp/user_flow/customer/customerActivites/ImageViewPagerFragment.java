package com.example.eatapp.user_flow.customer.customerActivites;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.eatapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImageViewPagerFragment extends Fragment {

    private ImageView pc;

    public ImageViewPagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_image_view_pager, container, false);

        pc = v.findViewById(R.id.img_view_home);
        int x = getArguments().getInt("someInt", 0);
        pc.setImageResource(x);
        return v;
    }

    public static ImageViewPagerFragment newInstance(int someInt) {
        ImageViewPagerFragment imageViewPagerFragment = new ImageViewPagerFragment();

        Bundle args = new Bundle();
        args.putInt("someInt", someInt);
        imageViewPagerFragment.setArguments(args);

        return imageViewPagerFragment;
    }

}
