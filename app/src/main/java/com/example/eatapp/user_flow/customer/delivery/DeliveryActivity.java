package com.example.eatapp.user_flow.customer.delivery;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.user_flow.customer.CustomerActivity;
import com.example.eatapp.user_flow.customer.TakeAwayActivity;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class DeliveryActivity extends AppCompatActivity {


    private Button bnbnForWhereDelivery;
    private final static int PERMISSION_ALL = 1;
    private TextView deliveryprice, totalPrec;
    private List<MenuItemModel> cartList = new ArrayList<>();
    //1 to get data from room
    private LocalDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        setUP();
    }

    private void setUP() {
        deliveryprice = findViewById(R.id.tv_price_delivery_numb);
        bnbnForWhereDelivery = findViewById(R.id.bn_for_where_delivery);
        totalPrec = findViewById(R.id.tv_toatl_price_delivery_numb);
        bnbnForWhereDelivery.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //check Self Permission at ACCESS_FINE_LOCATION  and ACCESS_COARSE_LOCATION
                if (ActivityCompat.checkSelfPermission(DeliveryActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        DeliveryActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    // if the Permission not GRANTED

                    // 2 check at status of permission if yes or no
                    //لو ما عندي هذا الاذن بدي اطلبو
                    ActivityCompat.requestPermissions(DeliveryActivity.this,

                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                            PERMISSION_ALL);


                    return;
                } else {
                    //او عندي ياهم انتقل
                    Intent intent = new Intent(DeliveryActivity.this, CustomerMap.class);
                    startActivity(intent);

//                    Intent intent = new Intent(DeliveryActivity.this,DeliviryMapAvtivity.class);
//                    startActivity(intent);

                }


            }
        });

        //api
        deliveryprice.setText("3");
        connectedLocalDataBase();


    }

    // 1 check at status of permission if yes or no
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ALL: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, Yes ! Do the
                    // contacts-related task you need to do.
                    Intent intent = new Intent(DeliveryActivity.this, CustomerMap.class);
                    startActivity(intent);
                } else {

                    // permission denied,NO! Disable the
                    // functionality that depends on this permission.

                    showDailogDeniedPermissions();

                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    //deffult dailog
    private void showDailogDeniedPermissions() {

        AlertDialog.Builder builder = new AlertDialog.Builder(DeliveryActivity.this);

        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.showDailogDeniedPermissions)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


    private void connectedLocalDataBase() {
        //2 to get data from room
        database = LocalDatabase.getAppDatabase(this);
        DeliveryActivity.GetDataOrderAyncTasl getGetDataOrderAyncTasl = new GetDataOrderAyncTasl();

        try {
            cartList.addAll(getGetDataOrderAyncTasl.execute().get());

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        totalPrec.setText(ConverDoubleToString.ConDoubleString(TotalFinal.sumDelivaryPrice(TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList)))));



        }


   ///////
    private class GetDataOrderAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }


    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }


}

