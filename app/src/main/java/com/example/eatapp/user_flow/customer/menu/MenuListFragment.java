package com.example.eatapp.user_flow.customer.menu;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eatapp.R;
import com.example.eatapp.network.models.menu_details.GetStoreDetailsItem;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuListFragment extends Fragment {

    private ArrayList<GetStoreDetailsItem> foodItemList =new ArrayList<>();

    private static final String KEY_FOOD_LIST = " KEY_FOOD_LIST";


    public MenuListFragment() {
        // Required empty public constructor
    }

    public static MenuListFragment newInstance(ArrayList<GetStoreDetailsItem> foodList) {

        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_FOOD_LIST, foodList);
        MenuListFragment fragment = new MenuListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            foodItemList = getArguments().getParcelableArrayList(KEY_FOOD_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_courses, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView_main_courses);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        //to get recycalAdapter
        MenuRecyclerAdapter adapter = new MenuRecyclerAdapter(foodItemList, getContext());
        recyclerView.setAdapter(adapter);

        return view;
    }


}
