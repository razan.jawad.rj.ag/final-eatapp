package com.example.eatapp.user_flow.manager;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FoodWithTableMaagerActivity extends AppCompatActivity {

    private RecyclerView recyclerViewOrderCustomer;
    private List<MenuItemModel> cartList = new ArrayList<>();
    private LocalDatabase database;

    private TextView tvNumberTable, tvLocationTable, tvSumTotal, tvNumberSumTotal;
    private Button bnOk;

    private double numberSumTotal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_with_table_maager);

        setUp();
    }

    private void setUp() {
        recyclerViewOrderCustomer = findViewById(R.id.recycalView_order_customer_foodwithtable);
        recyclerViewOrderCustomer.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));

        tvNumberTable = findViewById(R.id.tv_number_table_booktableManager);
        tvLocationTable = findViewById(R.id.tv_location_table_booktableManager);
        tvSumTotal = findViewById(R.id.tv_sum_of_otder_manager_booktableManager);
        tvNumberSumTotal = findViewById(R.id.tv_number_sum_of_otder_manager_booktableManager);

        bnOk = findViewById(R.id.bn_ok_manager_booktableManager);
        connectLocalDataRoom();

        //to convert double to format  string
        numberSumTotal = TotalFinal.totalPrice(cartList);
        //to put total in text
        tvNumberSumTotal.setText(ConverDoubleToString.ConDoubleString(numberSumTotal) + " " + "JD");
        tvLocationTable.setText(SharedPreferencesOfMe.gettablePlace(SharedPreferencesOfMe.Table_Place, this));
        tvNumberTable.setText("4");
        bnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void connectLocalDataRoom() {
        database = LocalDatabase.getAppDatabase(this);
        GetOrderManagerAyncTasl getOrderAyncTasl = new GetOrderManagerAyncTasl();

        //to call adapter
        OrderManagerRecyclerAdapter adapter = null;

        try {
            cartList = getOrderAyncTasl.execute().get();
            if (cartList.size() > 0) {//to put total in text


                adapter = new OrderManagerRecyclerAdapter((ArrayList<MenuItemModel>) cartList);
                recyclerViewOrderCustomer.setAdapter(adapter);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class GetOrderManagerAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }

    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }
}
