package com.example.eatapp.user_flow.customer.menu;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;

import com.example.eatapp.R;

public class ImageButtonHart extends android.support.v7.widget.AppCompatImageButton  {


    OnClickListener listener;
    public ImageButtonHart(Context context) {
        super(context);
    }

    public ImageButtonHart(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public ImageButtonHart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }



}
