package com.example.eatapp.user_flow.customer.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.CustomerActivity;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

public class MenuActivity extends AppCompatActivity {
    //1
    private ViewPager viewPager;
    /*private ArrayList<GetMenuDetailsItem> appetisersArray;
    private ArrayList<GetMenuDetailsItem> mainCoursesArray;
    private ArrayList<GetMenuDetailsItem> dessertsArray;
    private ArrayList<GetMenuDetailsItem> beveragesArray;*/

    private MenuPagerAdapterNew menuPagerAdapterNew;
    private static String PAGE_ID_KEY = "page_id_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
//2
        setupViewPager();
    }

    private void setupViewPager() {
//الشب البستوني
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MenuActivity.this, CustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 1);
                startActivity(intent);
                finish();
            }
        });

//        final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);
//
//        final MenuDetailsRequest menuDetailsRequest = new MenuDetailsRequest();
//        menuDetailsRequest.setMenuId("2");
//
//        Call<MenuDetailsResponsePojo> call = apiService.getMenuDetail(menuDetailsRequest);
//        call.enqueue(new Callback<MenuDetailsResponsePojo>() {
//
//            @Override
//            public void onResponse(Call<MenuDetailsResponsePojo> call, Response<MenuDetailsResponsePojo> response) {
//                ArrayList<GetMenuDetailsItem> companiesTypeItems = response.body().getGetMenuDetails();
//                for (int i = 0; i < companiesTypeItems.size(); i++) {
//                   // if (companiesTypeItems.get(i).getMenuId().equals("2")) {
//                        appetisersArray = response.body().getGetMenuDetails();


//                        MenuPagerAdapter menuPagerAdapter = new MenuPagerAdapter(getSupportFragmentManager(), MenuActivity.this,
//                                appetisersArray, appetisersArray, appetisersArray, appetisersArray);

        menuPagerAdapterNew = new MenuPagerAdapterNew(getSupportFragmentManager(), MenuActivity.this);
        //3 d
        viewPager = findViewById(R.id.view_pager);
        //1 to Use one adapter for multiple arrays
        // viewPager.setAdapter(menuPagerAdapter);
        viewPager.setAdapter(menuPagerAdapterNew);
        // 4 to change ViewPager's page
        viewPager.setCurrentItem(0);
        SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);

        // MenuDetailsRequest menuDetailsResponsePojo = new MenuDetailsRequest();
//                        menuDetailsRequest.setMenuId("1");
//                        Call<MenuDetailsResponsePojo> callDetail = apiService.getMenuDetail( menuDetailsRequest);
//
//                        callDetail.enqueue(new Callback<MenuDetailsResponsePojo>() {
//                            @Override
//                            public void onResponse(Call<MenuDetailsResponsePojo> call, Response<MenuDetailsResponsePojo> response) {
//                                appetisersArray = response.body().getGetMenuDetails();
//
//
//                                MenuPagerAdapter menuPagerAdapter = new MenuPagerAdapter(getSupportFragmentManager(), MenuActivity.this,
//                                        appetisersArray, appetisersArray, appetisersArray, appetisersArray);
//                                //3 d
//                                viewPager = findViewById(R.id.view_pager);
//                                //1 to Use one adapter for multiple arrays
//                                viewPager.setAdapter(menuPagerAdapter);
//                                // 4 to change ViewPager's page
//                                viewPager.setCurrentItem(0);
//                                SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
//                                viewPagerTab.setViewPager(viewPager);
//                            }
//
//                            @Override
//                            public void onFailure(Call<MenuDetailsResponsePojo> call, Throwable t) {
//                                Log.e("menu Details Error", t.toString());
//                            }
//                        });
        // }
    }


//            }
//
//            @Override
//            public void onFailure(Call<MenuDetailsResponsePojo> call, Throwable t) {
//                Log.e("read menu Error", t.toString());
//            }

            /*@Override
            public void onFailure(Call<MenuResponsePojo> call, Throwable t) {
                Log.e("notwork Error", t.toString());
            }
        });
        if (!response.body().getLogin().getCustomerId().equals("-1")) {
            Intent intent = new Intent(getContext(), CustomerActivity.class);
            startActivity(intent);
        } else {
            showDailoglogin();
        }*/
      /*  call.enqueue(new Callback<MenuDetailsResponsePojo>() {
            @Override
            public void onResponse(Call<MenuDetailsResponsePojo> call, Response<MenuDetailsResponsePojo> response) {
               ArrayList <GetMenuDetailsItem> detail = (ArrayList<GetMenuDetailsItem>) response.body().getGetMenuDetails();

                MenuPagerAdapter menuPagerAdapter = new MenuPagerAdapter(getSupportFragmentManager(), MenuActivity.this,
                         detail, detail, detail, detail);
                //3 d
                viewPager = findViewById(R.id.view_pager);
                //1 to Use one adapter for multiple arrays
                viewPager.setAdapter(menuPagerAdapter);
                // 4 to change ViewPager's page
                viewPager.setCurrentItem(0);
                SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
                viewPagerTab.setViewPager(viewPager);
            }

            @Override
            public void onFailure(Call<MenuDetailsResponsePojo> call, Throwable t) {
                Log.e("notwork Error",t.toString());
            }
        });*/


}







