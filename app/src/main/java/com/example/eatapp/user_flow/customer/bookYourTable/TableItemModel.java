package com.example.eatapp.user_flow.customer.bookYourTable;

import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

public class TableItemModel implements Parcelable {
    @PrimaryKey
    private int id;
    private String nameTable;
    private int numberTable;



    public TableItemModel() {

    }
    public TableItemModel(String nameTable, int numberTable,int id) {
        this.nameTable = nameTable;
        this.numberTable = numberTable;
        this.id=id;
    }

    protected TableItemModel(Parcel in) {
        nameTable = in.readString();
        numberTable = in.readInt();
        id= in.readInt();
    }

    public static final Creator<TableItemModel> CREATOR = new Creator<TableItemModel>() {
        @Override
        public TableItemModel createFromParcel(Parcel in) {
            return new TableItemModel(in);
        }

        @Override
        public TableItemModel[] newArray(int size) {
            return new TableItemModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getNameTable() {
        return nameTable;
    }

    public void setNameTable(String nameTable) {
        this.nameTable = nameTable;
    }

    public int getNumberTable() {
        return numberTable;
    }

    public void setNumberTable(int numberTable) {
        this.numberTable = numberTable;
    }

    public static Creator<TableItemModel> getCREATOR() {
        return CREATOR;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameTable);
        dest.writeDouble(numberTable);
    }
}
