package com.example.eatapp.user_flow.customer.Login;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.createAccount.CreateRequestsPojo;
import com.example.eatapp.network.models.createAccount.CreateResponsePojo;
import com.example.eatapp.user_flow.customer.CustomerActivity;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CreatAccountFragment extends Fragment {

    private String fullName, email, password, confirmPassword, phoneNamber;

    private Button bn_createAccont;
    private LocalDatabase localDatabase;

    private LocalDatabase dataBase;

    private ArrayList<LoginDataModel> informationArrayList;

    private EditText edFullName, edEmail, edPassword, edConfirmPassword, edPhoneNamber;

    private Spinner edCity;
    private String arabc[] = {"Enter city", "ادخل المدينه"};
    private String data[] = {arabc[0], "Amman", "Salt", "Zarqa", "Madaba"};
    private ArrayAdapter<String> adapter;
    private  String citySpinar="";

    public CreatAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_creat_account, container, false);
        edFullName = v.findViewById(R.id.ed_enter_full_name);
        edEmail = v.findViewById(R.id.ed_enter_email);
        edPassword = v.findViewById(R.id.ed_enter_password);
        edConfirmPassword = v.findViewById(R.id.ed_enter_confirm_password);
        edPhoneNamber = v.findViewById(R.id.ed_enter_phone);
       edCity = v.findViewById(R.id.ed_enter_city);
        bn_createAccont = v.findViewById(R.id.bn_create_account);

        adapter = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, data);
        edCity.setAdapter(adapter);
        edCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
              citySpinar=data[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                showDailogSpinar();
            }
        });


        bn_createAccont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //conected to servier
                if (IsInputValid()) {

                    connectedApi();

                }

            }
        });
        return v;
    }

    private void connectedApi() {
        CreateRequestsPojo createRequestsPojo = new CreateRequestsPojo();
        createRequestsPojo.setUserName(edFullName.getText().toString());
        createRequestsPojo.setEmail(edEmail.getText().toString());
        createRequestsPojo.setPassword(edPassword.getText().toString());
        createRequestsPojo.setCityNameEn(citySpinar);
        createRequestsPojo.setPhone(edPhoneNamber.getText().toString());

        Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

        Call<CreateResponsePojo> call = apiService.addCustomer(createRequestsPojo);
        call.enqueue(new Callback<CreateResponsePojo>() {
            @Override
            public void onResponse(Call<CreateResponsePojo> call, Response<CreateResponsePojo> response) {
                if (response.body().getResponse().getResponseCode() == 0) {
                    Intent intent = new Intent(getContext(), CustomerActivity.class);
                    //to make id sharing SharedPreferences
                    String id = response.body().getCustomerId();
                    boolean loged=true;
                    SharedPreferencesOfMe.setCustomerIDDefaults(SharedPreferencesOfMe.CUSTOMET_ID, id, getContext());
                    SharedPreferencesOfMe.setISLogedDefaults(SharedPreferencesOfMe.IS_LOGED, loged, getContext());


                    //end SharedPreferences

                    startActivity(intent);
                } else {
                    showDailoglogin(response.body().getResponse().getMessage());
                }


            }

            @Override
            public void onFailure(Call<CreateResponsePojo> call, Throwable t) {
                showDailoglogin("somthing error");
            }
        });
    }


//        //to movig from CreatAccount fragment to manager or customer aactivity
//        @Override
//        protected void onPostExecute(Void aVoid) {
//            super.onPostExecute(aVoid);
//            if (isManager.isChecked() == true) {
//                Intent i = new Intent(getContext(), ManagerActivity.class);
//                startActivity(i);
//                getActivity().finish();
//            } else {
//                Intent i = new Intent(getContext(), CustomerActivity.class);
//                startActivity(i);
//                getActivity().finish();
//            }
//
//        }
//

    //chek if theEmpty
    private boolean IsInputValid() {
        fullName = edFullName.getText().toString();
       email = edEmail.getText().toString();
        password = edPassword.getText().toString();
        confirmPassword = edConfirmPassword.getText().toString();
        phoneNamber = edPhoneNamber.getText().toString();


        if (fullName.isEmpty() && email.isEmpty() && password.isEmpty() &&
                confirmPassword.isEmpty() && phoneNamber.isEmpty())
        {
            Toast.makeText(getContext(), R.string.your_information_empty, Toast.LENGTH_SHORT).show();

            return false;
        } else if (TextUtils.isEmpty(fullName)) {
            edFullName.setError(getContext().getString(R.string.error_FullName_is_empty));
            return false;
        } else if (TextUtils.isEmpty(email)) {
            edEmail.setError(getContext().getString(R.string.error_email_is_empty));
            return false;
        }
        else if (TextUtils.isEmpty(password)) {
            edPassword.setError(getContext().getString(R.string.your_password_is_empty));

            return false;
        }
        else if (TextUtils.isEmpty(confirmPassword)) {

            edConfirmPassword.setError(getContext().getString(R.string.your_com_password_is_empty));

            return false;
        }
        else if (TextUtils.isEmpty(phoneNamber)) {
            edPhoneNamber.setError(getContext().getString(R.string.error_phone_is_empty));
            return false;
        }else if( citySpinar.equals("Enter city")||citySpinar.equals("ادخل المدينه"))
        {
            showDailogSpinar();
            return false;
        }
        else if(confirmPasswordChek()){
            edConfirmPassword.setError(getContext().getString(R.string.conffirm));
            return false;
        }
            return true;
    }

    //chek if the password and confirm same
    private boolean confirmPasswordChek() {
       if( password.equals(confirmPassword))
           return false;
       return true;
    }

    //deffult dailog
    private void showDailoglogin(String error) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(error)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.show();
    }

    //deffult dailog
    private void showDailogSpinar() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.youDontSelectItem)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }
}