package com.example.eatapp.user_flow.manager;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.ReadManagerDilevaryPrice.DeliveryPriceAndPhoneRequst;
import com.example.eatapp.network.models.ReadManagerDilevaryPrice.DeliveryPriceAndPhoneResponse;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryManagerActivity extends AppCompatActivity {

    private RecyclerView recyclerViewOrderCustomer;
    private List<MenuItemModel> cartList = new ArrayList<>();
    private LocalDatabase database;

    private TextView phoneCustomer, tvSumTotal, tvNumberSumTotal;
    private Button bnOk, locationCustomer;

    private double numberSumTotal;

    final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);
    private String customerId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_manager);

        setUp();
    }

    private void setUp() {
        customerId = SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this);
        recyclerViewOrderCustomer = findViewById(R.id.recycalView_order_customer_delivarymanager);
        recyclerViewOrderCustomer.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));

        locationCustomer = findViewById(R.id.tv_locationCustomer_delivaryManager);
        phoneCustomer = findViewById(R.id.tv_phoneCustomer_delivaryManager);
        tvSumTotal = findViewById(R.id.tv_sum_of_otder_manager_booktableManager);
        tvNumberSumTotal = findViewById(R.id.tv_number_sum_of_otder_manager_DelManager);

        bnOk = findViewById(R.id.bn_ok_manager_booktableManager);
        connectLocalDataRoom();


        //to convert double to format  string
        numberSumTotal = TotalFinal.totalPrice(cartList);
        //to put total in text
        //tvNumberSumTotal.setText(ConverDoubleToString.ConDoubleString(numberSumTotal) + " " + "JD");

        bnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        locationCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //need api
                double latitude = 32.02392574271297;
                double longitude = 35.71916889399291;

                Uri gmmIntentUri = Uri.parse("google.navigation:q=" + latitude + "," + longitude);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
        DeliveryPriceAndPhoneRequst deliveryPriceAndPhoneRequst = new DeliveryPriceAndPhoneRequst();
        deliveryPriceAndPhoneRequst.setCustomerId(customerId);
        deliveryPriceAndPhoneRequst.setOrderTypeId("1");
        Call<DeliveryPriceAndPhoneResponse> callDeliveryPriceAndPhoneResponse = apiService.prcieAndPhoneDelivary(deliveryPriceAndPhoneRequst);
        callDeliveryPriceAndPhoneResponse.enqueue(new Callback<DeliveryPriceAndPhoneResponse>() {
            @Override
            public void onResponse(Call<DeliveryPriceAndPhoneResponse> call, Response<DeliveryPriceAndPhoneResponse> response) {
                if (response.body().getReadingCustomerOrderDetails().getFinalPrice() == null) {
                    tvNumberSumTotal.setText("0.0");
                } else {
                    tvNumberSumTotal.setText(response.body().getReadingCustomerOrderDetails().getFinalPrice() + " " + "JD");
                }
                phoneCustomer.setText(response.body().getReadingCustomerOrderDetails().getPhone());
            }

            @Override
            public void onFailure(Call<DeliveryPriceAndPhoneResponse> call, Throwable t) {
                Toast.makeText(DeliveryManagerActivity.this, "eeror in notwork", Toast.LENGTH_SHORT).show();

            }
        });


    }


    private void connectLocalDataRoom() {
        database = LocalDatabase.getAppDatabase(this);
        GetOrderManagerAyncTasl getOrderAyncTasl = new GetOrderManagerAyncTasl();

        //to call adapter
        OrderManagerRecyclerAdapter adapter = null;

        try {
            cartList = getOrderAyncTasl.execute().get();
            if (cartList.size() > 0) {//to put total in text


                adapter = new OrderManagerRecyclerAdapter((ArrayList<MenuItemModel>) cartList);
                recyclerViewOrderCustomer.setAdapter(adapter);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class GetOrderManagerAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }

    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }
}
