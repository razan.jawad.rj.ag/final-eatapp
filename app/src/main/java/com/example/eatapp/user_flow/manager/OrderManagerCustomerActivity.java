package com.example.eatapp.user_flow.manager;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.managerReadOrderDetails.OrderDetailsRequst;
import com.example.eatapp.network.models.managerReadOrderDetails.OrderDetailsResponse;
import com.example.eatapp.network.models.managerReadOrderDetails.ReadingOrder;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderManagerCustomerActivity extends AppCompatActivity {
//type order =1

    private RecyclerView recyclerViewOrderCustomer;
    private List<MenuItemModel> cartList = new ArrayList<>();
    private List<ReadingOrder> orderList = new ArrayList<>();

    private TextView tvNumberSumTotal, tvSumTotal;
    private Button bnOK;
    private static String PAGE_ID_KEY = "page_id_key";

    private double numberSumTotal;

    private LocalDatabase database;

    final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_manager_customer);
        setUp();
    }

    private void setUp() {

        recyclerViewOrderCustomer = findViewById(R.id.recycalView_order_customer_foodwithtable);

        recyclerViewOrderCustomer.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));

        tvSumTotal = findViewById(R.id.tv_sum_of_otder_manager_booktableManager);
        tvNumberSumTotal = findViewById(R.id.tv_number_sum_of_otder_manager_booktableManager);
        bnOK = findViewById(R.id.bn_ok_manager_booktableManager);

        connectLocalDataRoom();

//to convert double to format  string
        numberSumTotal = TotalFinal.totalPrice(cartList);
        //to put total in text
        tvNumberSumTotal.setText(ConverDoubleToString.ConDoubleString(numberSumTotal) + " " + "JD");

        bnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        OrderDetailsRequst orderDetailsRequst = new OrderDetailsRequst();
        String orderType = getIntent().getExtras().getString("OrderType");
        orderDetailsRequst.setOrderTypeId(orderType);
        String customerId = SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this);
        orderDetailsRequst.setCustomerId(customerId);
        Call<OrderDetailsResponse>callOrderDetails= apiService.OrderDetails(orderDetailsRequst);
        callOrderDetails.enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {

            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {

            }
        });

    }

    private void connectLocalDataRoom() {
        database = LocalDatabase.getAppDatabase(this);
        GetOrderManagerAyncTasl getOrderAyncTasl = new GetOrderManagerAyncTasl();

        //to call adapter
        OrderManagerRecyclerAdapter adapter = null;

        try {
            cartList = getOrderAyncTasl.execute().get();
            if (cartList.size() > 0) {//to put total in text

                adapter = new OrderManagerRecyclerAdapter((ArrayList<MenuItemModel>) cartList);
                recyclerViewOrderCustomer.setAdapter(adapter);
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class GetOrderManagerAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }

    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }
}
