package com.example.eatapp.user_flow.customer.delivery;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.example.eatapp.network.googleApi.ApiGoogleClient;
import com.example.eatapp.network.googleApi.IgoogleApiEndPoint;

public class ShopingInformation {
    private Double lat, lng;
    public static final String BASE_URL = "http://maps.googleapis.com";

    public ShopingInformation() {

    }

    public ShopingInformation(Double lat, Double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }


    public static IgoogleApiEndPoint getGeoIgoogleApiEndPoint() {
        return ApiGoogleClient.getClient(BASE_URL).create(IgoogleApiEndPoint.class);
    }

    public static Bitmap scaleBitmap (Bitmap bitmap,int newWidth,int newHeight)
        {
       Bitmap  scaledBitmap=Bitmap.createBitmap(newWidth,newHeight,Bitmap.Config.ARGB_8888);

       float scaleX=newWidth/(float)bitmap.getWidth();
       float scaleY=newHeight/(float)bitmap.getHeight();
       float pivotX=0,pivotY=0;

            Matrix scaleMatrix=new Matrix();
            scaleMatrix.setScale(scaleX,scaleY,pivotX,pivotY);

            Canvas canvas=new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bitmap,0,0,new Paint(Paint.FILTER_BITMAP_FLAG));

            return scaledBitmap;
    }

}
