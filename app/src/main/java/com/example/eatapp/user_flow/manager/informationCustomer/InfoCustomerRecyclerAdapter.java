package com.example.eatapp.user_flow.manager.informationCustomer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eatapp.R;
import com.example.eatapp.network.models.readCustomersOrders.GetCustomerOrdersItem;
import com.example.eatapp.network.models.readCustomersOrders.ReadCustomersOrdersResponse;
import com.example.eatapp.user_flow.manager.BookTableManagerActivity;
import com.example.eatapp.user_flow.manager.DeliveryManagerActivity;
import com.example.eatapp.user_flow.manager.FoodWithTableMaagerActivity;
import com.example.eatapp.user_flow.manager.OrderManagerCustomerActivity;

import java.util.ArrayList;

import retrofit2.Callback;


public class InfoCustomerRecyclerAdapter extends RecyclerView.Adapter<InfoCustomerRecyclerAdapter.ViewItem> {

    private ArrayList<GetCustomerOrdersItem> items;

    private Callback<ReadCustomersOrdersResponse> context;
    private int id;

    private static String PAGE_ID_KEY = "page_id_key";


    public InfoCustomerRecyclerAdapter(ArrayList<GetCustomerOrdersItem> item,
                                       Callback<ReadCustomersOrdersResponse> context, int id) {
        this.items = item;
        this.context = context;
        this.id = id;

    }

    //onCreateViewHolder used to HAndle on Clicks
    @Override
    public InfoCustomerRecyclerAdapter.ViewItem onCreateViewHolder(final ViewGroup parent, int viewType) {

        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_info_customer, parent, false);


        return new InfoCustomerRecyclerAdapter.ViewItem(itemView);

    }

    //to fill each item with data from the array depending on position
    @Override
    public void onBindViewHolder(final InfoCustomerRecyclerAdapter.ViewItem holder, final int position) {
        GetCustomerOrdersItem currentItem = items.get(position);

        holder.tvNameCustomer.setText(currentItem.getCustomerNameEn());
        holder.tvPhone.setText(String.valueOf(currentItem.getPhone()));
        holder.tvEmail.setText(String.valueOf(currentItem.getEmail()));


        //on click at the row in recycal
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int idRow = Integer.parseInt(items.get(position).getOrderTypeId()); // get Id
                if (id == 1) {
                    Intent intent = new Intent(holder.itemView.getContext(), OrderManagerCustomerActivity.class);
                    intent.putExtra("pos", idRow); // Pass Id

                    intent.putExtra("OrderType",items.get(position).getOrderTypeId());
                    //nead cusromer id
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    holder.itemView.getContext().startActivity(intent);
                } else if (id == 2) {
                    Intent intent = new Intent(holder.itemView.getContext(), FoodWithTableMaagerActivity.class);
                    intent.putExtra("pos", idRow); // Pass Id
                    //intent.putExtra("CustomerId",items.get(position).getOrderTypeId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    holder.itemView.getContext().startActivity(intent);
                } else if (id == 3) {
                    Intent intent = new Intent(holder.itemView.getContext(), BookTableManagerActivity.class);
                    intent.putExtra("pos", idRow); // Pass Id
                    //intent.putExtra("CustomerId",items.get(position).getOrderTypeId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    holder.itemView.getContext().startActivity(intent);
                } else if (id == 4) {
                    Intent intent = new Intent(holder.itemView.getContext(), DeliveryManagerActivity.class);
                    intent.putExtra("pos", idRow); // Pass Id
                   // intent.putExtra("CustomerId",items.get(position).getOrderTypeId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    holder.itemView.getContext().startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;

        return items.size();
    }


    //The View Item part responsible for connecting the row.xml with
    // each item in the RecyclerView
    //make declare and initalize
    public class ViewItem extends RecyclerView.ViewHolder {

        //Declare
        TextView tvNameCustomer, tvPhone, tvEmail;

        //initialize
        public ViewItem(View itemView) {
            super(itemView);
            tvNameCustomer = itemView.findViewById(R.id.tv_name_customer);
            tvPhone = itemView.findViewById(R.id.tv_phone_customer);
            tvEmail = itemView.findViewById(R.id.tv_email_customer);


        }
    }

}
