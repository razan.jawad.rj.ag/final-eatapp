package com.example.eatapp.user_flow.manager.informationCustomer;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.readCustomersOrders.GetCustomerOrdersItem;
import com.example.eatapp.network.models.readCustomersOrders.ReadCustomersOrdersRequest;
import com.example.eatapp.network.models.readCustomersOrders.ReadCustomersOrdersResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InformationCustomerActivity extends AppCompatActivity {

    private RecyclerView recyclerViewInformationCustomer;
    private SwipeRefreshLayout mySwipeRefreshLayout;

    private List<GetCustomerOrdersItem> informatiomCustomer = new ArrayList<>();

    final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_customer);
        setUp();

    }

    private void setUp() {
        mySwipeRefreshLayout = findViewById(R.id.swiperefresh);

        recyclerViewInformationCustomer = findViewById(R.id.recycalView_Inf_customer);
        recyclerViewInformationCustomer.setLayoutManager(new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false));
        //if Toast.makeText(this, "No Order from Customer", Toast.LENGTH_SHORT).show();

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getList();
                    }
                }
        );

        getList();


    }

    private void getList() {
        mySwipeRefreshLayout.setRefreshing(true);

        if (getIntent().getExtras() != null) {
            int pageId = getIntent().getExtras().getInt("page_id_key");
            if (pageId == 1) {


                ReadCustomersOrdersRequest readCustomersOrdersRequest = new ReadCustomersOrdersRequest();
                readCustomersOrdersRequest.setOrderTypeId("1");
                Call<ReadCustomersOrdersResponse> callTackeAway = apiService.readCustomersOrders(readCustomersOrdersRequest);
                callTackeAway.enqueue(new Callback<ReadCustomersOrdersResponse>() {
                    @Override
                    public void onResponse(Call<ReadCustomersOrdersResponse> call, Response<ReadCustomersOrdersResponse> response) {
                        List<GetCustomerOrdersItem> customerOrdersInf = response.body().getGetCustomerOrders();

                        informatiomCustomer = response.body().getGetCustomerOrders();

                        for (int j = 0; j < informatiomCustomer.size(); j++) {
                            InfoCustomerRecyclerAdapter customerRecyclerAdapter = new InfoCustomerRecyclerAdapter((ArrayList<GetCustomerOrdersItem>) informatiomCustomer, this, 1);
                            recyclerViewInformationCustomer.setAdapter(customerRecyclerAdapter);
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                        mySwipeRefreshLayout.setRefreshing(false);
                    }


                    @Override
                    public void onFailure(Call<ReadCustomersOrdersResponse> call, Throwable t) {

                    }
                });

            } else if (pageId == 2) {


                ReadCustomersOrdersRequest readCustomersOrdersRequest = new ReadCustomersOrdersRequest();
                readCustomersOrdersRequest.setOrderTypeId("2");
                Call<ReadCustomersOrdersResponse> callTackeAway = apiService.readCustomersOrders(readCustomersOrdersRequest);
                callTackeAway.enqueue(new Callback<ReadCustomersOrdersResponse>() {
                    @Override
                    public void onResponse(Call<ReadCustomersOrdersResponse> call, Response<ReadCustomersOrdersResponse> response) {
                        List<GetCustomerOrdersItem> customerOrdersInf = response.body().getGetCustomerOrders();

                        informatiomCustomer = response.body().getGetCustomerOrders();

                        for (int j = 0; j < informatiomCustomer.size(); j++) {
                            InfoCustomerRecyclerAdapter customerRecyclerAdapter = new InfoCustomerRecyclerAdapter((ArrayList<GetCustomerOrdersItem>) informatiomCustomer, this, 2);
                            recyclerViewInformationCustomer.setAdapter(customerRecyclerAdapter);
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                        mySwipeRefreshLayout.setRefreshing(false);
                    }


                    @Override
                    public void onFailure(Call<ReadCustomersOrdersResponse> call, Throwable t) {

                    }
                });
            } else if (pageId == 3) {

                ReadCustomersOrdersRequest readCustomersOrdersRequest = new ReadCustomersOrdersRequest();
                readCustomersOrdersRequest.setOrderTypeId("3");
                Call<ReadCustomersOrdersResponse> callTackeAway = apiService.readCustomersOrders(readCustomersOrdersRequest);
                callTackeAway.enqueue(new Callback<ReadCustomersOrdersResponse>() {
                    @Override
                    public void onResponse(Call<ReadCustomersOrdersResponse> call, Response<ReadCustomersOrdersResponse> response) {
                        List<GetCustomerOrdersItem> customerOrdersInf = response.body().getGetCustomerOrders();

                        informatiomCustomer = response.body().getGetCustomerOrders();

                        for (int j = 0; j < informatiomCustomer.size(); j++) {
                            InfoCustomerRecyclerAdapter customerRecyclerAdapter = new InfoCustomerRecyclerAdapter((ArrayList<GetCustomerOrdersItem>) informatiomCustomer, this, 3);
                            recyclerViewInformationCustomer.setAdapter(customerRecyclerAdapter);
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                        mySwipeRefreshLayout.setRefreshing(false);
                    }


                    @Override
                    public void onFailure(Call<ReadCustomersOrdersResponse> call, Throwable t) {

                    }
                });
            } else {


                ReadCustomersOrdersRequest readCustomersOrdersRequest = new ReadCustomersOrdersRequest();
                readCustomersOrdersRequest.setOrderTypeId("4");
                Call<ReadCustomersOrdersResponse> callTackeAway = apiService.readCustomersOrders(readCustomersOrdersRequest);
                callTackeAway.enqueue(new Callback<ReadCustomersOrdersResponse>() {
                    @Override
                    public void onResponse(Call<ReadCustomersOrdersResponse> call, Response<ReadCustomersOrdersResponse> response) {
                        List<GetCustomerOrdersItem> customerOrdersInf = response.body().getGetCustomerOrders();

                        informatiomCustomer = response.body().getGetCustomerOrders();

                        for (int j = 0; j < informatiomCustomer.size(); j++) {
                            InfoCustomerRecyclerAdapter customerRecyclerAdapter = new InfoCustomerRecyclerAdapter((ArrayList<GetCustomerOrdersItem>) informatiomCustomer, this, 4);
                            recyclerViewInformationCustomer.setAdapter(customerRecyclerAdapter);
                            mySwipeRefreshLayout.setRefreshing(false);
                        }
                        mySwipeRefreshLayout.setRefreshing(false);
                    }


                    @Override
                    public void onFailure(Call<ReadCustomersOrdersResponse> call, Throwable t) {

                    }
                });
            }

        }
    }
}
//    private void getList() {
//        // TODO implement a refresh
//        mySwipeRefreshLayout.setRefreshing(false); // Disables the refresh icon
//    }


//    private ArrayList<LoginDataModel> takeAway() {
//        ArrayList<LoginDataModel> m = new ArrayList<>();
//
//        LoginDataModel p1 = new LoginDataModel();
//        p1.setFullName("mohammed");
//        p1.setPhoneNamber("0785235469");
//        p1.setEmail("mohammed@gmail.com");
//        m.add(p1);
//
//        p1 = new LoginDataModel();
//        p1.setFullName("razan mohammed");
//        p1.setPhoneNamber("0788536282");
//        p1.setEmail("razan@gmail.com");
//        m.add(p1);
//        p1 = new LoginDataModel();
//        p1.setFullName(" Dania  mohammed");
//        p1.setPhoneNamber("0780123256");
//        p1.setEmail("dania@gmail.com");
//        m.add(p1);
//
//
//        p1 = new LoginDataModel();
//        p1.setFullName("mohammed mohammed");
//        p1.setPhoneNamber("07852369036");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        return m;
//    }
//
//    private ArrayList<LoginDataModel> foodWithTable() {
//        ArrayList<LoginDataModel> m = new ArrayList<>();
//
//        LoginDataModel p1 = new LoginDataModel();
//        p1.setFullName("Ahmad ahmad");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        p1 = new LoginDataModel();
//        p1.setFullName("Ahmad Ahmad");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//        p1 = new LoginDataModel();
//        p1.setFullName("Ahmad ahmad");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//
//        p1 = new LoginDataModel();
//        p1.setFullName("Ahmad ahmad");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        return m;
//    }
//
//    private ArrayList<LoginDataModel> bookTable() {
//        ArrayList<LoginDataModel> m = new ArrayList<>();
//
//        LoginDataModel p1 = new LoginDataModel();
//        p1.setFullName("rawan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        p1 = new LoginDataModel();
//        p1.setFullName("rawan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//        p1 = new LoginDataModel();
//        p1.setFullName("rawan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//
//        p1 = new LoginDataModel();
//        p1.setFullName("rawan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        return m;
//    }
//
//    private ArrayList<LoginDataModel> delivary() {
//        ArrayList<LoginDataModel> m = new ArrayList<>();
//
//        LoginDataModel p1 = new LoginDataModel();
//        p1.setFullName("razan razan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        p1 = new LoginDataModel();
//        p1.setFullName("razan razan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//        p1 = new LoginDataModel();
//        p1.setFullName("razan razan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//
//        p1 = new LoginDataModel();
//        p1.setFullName("razan razan");
//        p1.setPhoneNamber("07852369");
//        p1.setEmail("asdfgh@gmail.com");
//        m.add(p1);
//
//        return m;
//    }
//}
