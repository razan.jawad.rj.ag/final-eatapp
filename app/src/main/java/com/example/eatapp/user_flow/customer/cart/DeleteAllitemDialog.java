package com.example.eatapp.user_flow.customer.cart;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeleteAllitemDialog extends DialogFragment {
    private TextView tvSure;
    private Button bnYes, bnCansel;
    private List<MenuItemModel> cartList = new ArrayList<>();
    private DeletAllitemCartInterface deletAllitemCartInterface;

    //1 to get data from room
    private LocalDatabase database;


    public DeleteAllitemDialog() {
        // Required empty public constructor
    }


    public void setButtonsListener(DeletAllitemCartInterface deletAllitemCartInterface) {
        this.deletAllitemCartInterface = deletAllitemCartInterface;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_delete_allitem_dialog, container, false);

        setUp(v);

        return v;
    }

    private void setUp(View v) {
        tvSure = v.findViewById(R.id.tv_text_make_sure);
        bnYes = v.findViewById(R.id.bn_yas_delet_cart);
        bnCansel = v.findViewById(R.id.bn_cancel_delet_cart);

        bnCansel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        bnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletAllitemCartInterface.onClick();
                dismiss();
            }
        });
    }


}
