package com.example.eatapp.user_flow.customer;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.cart.CartFragment;
import com.example.eatapp.user_flow.customer.customerActivites.HomeFragment;
import com.example.eatapp.user_flow.customer.favorte.FavortFragment;

public class CustomerActivity extends AppCompatActivity {


    private ActionBar toolbar;
    private BottomNavigationView navigation;
    private int carentItrm = -1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        setUp();


    }

    private void setUp() {



        toolbar = getSupportActionBar();

        navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        if (getIntent().getExtras() != null) {
            int pageId = getIntent().getExtras().getInt("page_id_key");
            if (pageId == 1) {
                showCartFragment();

            } else if (pageId == 2) {
                showHomeFragment();
            }
        } else {
            showHomeFragment();
        }
    }

    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    showHomeFragment();
                    return true;
                case R.id.navigation_cart:
                    showCartFragment();
                    return true;
                case R.id.navigation_favorite:
                    showFavoriteFragment();
                    return true;
            }
            return false;
        }
    };

    private void showFavoriteFragment() {
        Fragment fragment;
        carentItrm = R.id.navigation_favorite;
        fragment = new FavortFragment();
        loadFragment(fragment);
    }

    //to know id of fragment make object to start the transaction
    private void showHomeFragment() {
        carentItrm = R.id.navigation_home;
        loadFragment(new HomeFragment());
    }

    private void showCartFragment() {
        carentItrm = R.id.navigation_cart;
        loadFragment(new CartFragment());
    }

    //to make transaction of fragment
    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    //to BackPressed  go Out the app
    @Override
    public void onBackPressed() {

        if (carentItrm != -1) {
            Fragment fragment;
            switch (carentItrm) {
                case R.id.navigation_cart:
                    showHomeFragment();
                    break;
                case R.id.navigation_favorite:
                    showHomeFragment();
                    break;
                case R.id.navigation_home:
                    this.finishAffinity();
                    break;
                default:
                    showHomeFragment();
                    break;
            }
        }
    }




}








