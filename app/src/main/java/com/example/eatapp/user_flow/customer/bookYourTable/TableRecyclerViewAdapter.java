package com.example.eatapp.user_flow.customer.bookYourTable;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.network.models.avaibilityTable.GetDiningTableItem;

import java.util.List;

public class TableRecyclerViewAdapter extends RecyclerView.Adapter<TableRecyclerViewAdapter.ViewItem> {

    private List<GetDiningTableItem> items;
    private static String PAGE_ID_KEY = "page_id_key";
    private SelectTableInterface selectTableInterface;

    public TableRecyclerViewAdapter(SelectTableInterface selectTableInterface) {
        this.selectTableInterface = selectTableInterface;

    }


    public void setArrayList(List<GetDiningTableItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public TableRecyclerViewAdapter.ViewItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_item_table, viewGroup, false);


        return new TableRecyclerViewAdapter.ViewItem(itemView);
    }

//    @Override
//    public void onBindViewHolder(@NonNull ViewItem viewItem, int i) {
//
//    }

    @Override
    public void onBindViewHolder(final TableRecyclerViewAdapter.ViewItem holder, final int position) {
        GetDiningTableItem currentItem = items.get(position);

        holder.tvNameTable.setText(holder.context.getString(R.string.Tableplace) + " :" + currentItem.getTablePlace());
        holder.tvNumberTable.setText(holder.context.getString(R.string.Tablenumber) + " :" + String.valueOf(currentItem.getChairsCount()));

        SharedPreferencesOfMe.setTablePlaceDefaults(SharedPreferencesOfMe.Table_Place, items.get(position).getTablePlace(), holder.context);

        //on click at the row in recycal
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTableInterface.onClick(items.get(position));

            }
        });


    }

    @Override
    public int getItemCount() {
        if (items == null)
            return 0;

        return items.size();
    }


    public class ViewItem extends RecyclerView.ViewHolder {

        //Declare
        TextView tvNameTable, tvNumberTable, tvLocationTable;
        Context context;

        //initialize
        public ViewItem(View itemView) {
            super(itemView);
            context = itemView.getContext();
            tvNameTable = itemView.findViewById(R.id.tv_name_table);
            tvNumberTable = itemView.findViewById(R.id.tv_number_table_check);
            tvLocationTable = itemView.findViewById(R.id.tv_description_table);
            tvNumberTable.setVisibility(View.GONE);
        }
    }
    //2 to get data from room

}