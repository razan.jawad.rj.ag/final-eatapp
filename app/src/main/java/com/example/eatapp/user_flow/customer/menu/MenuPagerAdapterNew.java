package com.example.eatapp.user_flow.customer.menu;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.eatapp.R;

public class MenuPagerAdapterNew extends FragmentPagerAdapter {
    private Context context;

    public MenuPagerAdapterNew(FragmentManager fm,Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: MainCoursesFragment mainCoursesFragment = new MainCoursesFragment();
                return mainCoursesFragment;

            case 1:
                AppetisersFragment appetisersFragment = new AppetisersFragment();
                return appetisersFragment;

            case 2:
                DessertsFragment dessertsFragment = new  DessertsFragment();
                return dessertsFragment;
            case 3:
                BeverageskFragment beverageskFragment = new BeverageskFragment();
                return beverageskFragment;
        }
        return null;

    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                // 4 to chang name  of tab from english to arabic
                return context.getString(R.string.main_courses);

            case 1:
                return context.getString(R.string.appetisers);

            case 2:
                return context.getString(R.string.desserts);

            case 3:
                return context.getString(R.string.beverages);

        }
        return null;
    }
}

