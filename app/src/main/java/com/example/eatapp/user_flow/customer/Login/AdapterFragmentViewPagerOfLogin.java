package com.example.eatapp.user_flow.customer.Login;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.eatapp.R;

public class AdapterFragmentViewPagerOfLogin extends FragmentPagerAdapter {

    // 1 to chang name  of tab from english to arabic
    private Context context ;

    public AdapterFragmentViewPagerOfLogin(FragmentManager fm, Context context) {
        super(fm);
        // 2 to chang name  of tab from english to arabic
        this.context = context;
    }
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                LoginFragment loginFragment = new LoginFragment();
                return loginFragment;
            case 1:
                CreatAccountFragment creatAccountFragment= new CreatAccountFragment();
                return creatAccountFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.login);
            case 1:
                return context.getString(R.string.create_account);

        }
        return null;
    }

}
