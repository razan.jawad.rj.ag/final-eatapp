package com.example.eatapp.user_flow.customer.favorte;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.menu_details.GetStoreDetailsItem;
import com.example.eatapp.network.models.menu_details.MenuDetailsRequest;
import com.example.eatapp.network.models.menu_details.MenuDetailsResponsePojo;
import com.example.eatapp.network.models.readAllFavorite.GetFavoriteDetailsItem;
import com.example.eatapp.network.models.readAllFavorite.ReadFavoriteRequest;
import com.example.eatapp.network.models.readAllFavorite.ReadFavoriteResponse;
import com.example.eatapp.user_flow.customer.CustomerActivity;
import com.example.eatapp.user_flow.customer.menu.MenuActivity;
import com.example.eatapp.user_flow.customer.menu.MenuRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavortFragment extends Fragment {

    private RecyclerView recyclerView;
    private FavoriteRecyclerAdapter adapter;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private  FloatingActionButton fab;
    private static String PAGE_ID_KEY = "page_id_key";

    public FavortFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v=inflater.inflate(R.layout.fragment_favort, container, false);
        setUP(v);

    return v;}

    private void setUP(View v) {
        mySwipeRefreshLayout = v.findViewById(R.id.swiperefreshFavart);

        recyclerView = v.findViewById(R.id.recycalViewFavart);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        connectionAPI();


        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        connectionAPI();
                    }
                }
        );

       fab = v.findViewById(R.id.fab_f);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 1);
                startActivity(intent);

            }
        });

    }

    private void connectionAPI() {
        mySwipeRefreshLayout.setRefreshing(true);

        //MenuResponsePojo menuResponsePojo = new MenuResponsePojo();
        final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

        //final MenuDetailsRequest menuDetailsRequest = new MenuDetailsRequest();
       final ReadFavoriteRequest favoriteRequest=new ReadFavoriteRequest();

      //  menuDetailsRequest.setMenuId("2");

        Call<ReadFavoriteResponse> call = apiService.readFavorite();
       call.enqueue(new Callback<ReadFavoriteResponse>() {
           @Override
           public void onResponse(Call<ReadFavoriteResponse> call, Response<ReadFavoriteResponse> response) {
               List<GetFavoriteDetailsItem>getFavoriteDetailsItems=response.body().getGetFavoriteDetails();
               for (int i = 0; i <getFavoriteDetailsItems.size() ; i++) {
                   adapter= new FavoriteRecyclerAdapter((ArrayList<GetFavoriteDetailsItem>) response.body().getGetFavoriteDetails(), getContext());
                   recyclerView.setAdapter(adapter);
                   mySwipeRefreshLayout.setRefreshing(false);



               }
               mySwipeRefreshLayout.setRefreshing(false);
           }

           @Override
           public void onFailure(Call<ReadFavoriteResponse> call, Throwable t) {
               Toast.makeText(getContext(), "error in Notwork", Toast.LENGTH_SHORT).show();
               mySwipeRefreshLayout.setRefreshing(true);
           }
       });

    }

}
