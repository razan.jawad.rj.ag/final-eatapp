package com.example.eatapp.user_flow.customer.cart;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.FinalString;
import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.getOffer.GetOfferItem;
import com.example.eatapp.network.models.getOffer.ReadOfferRequst;
import com.example.eatapp.network.models.getOffer.ReadOfferResponse;
import com.example.eatapp.network.models.readRestaurantInf.ReadRetaurantinformationResponse;
import com.example.eatapp.network.models.updateOrder.UpdateOrderReqest;
import com.example.eatapp.network.models.updateOrder.UpdateOrderResponse;
import com.example.eatapp.user_flow.customer.TakeAwayActivity;
import com.example.eatapp.user_flow.customer.bookYourTable.BookYourTableActivity;
import com.example.eatapp.user_flow.customer.delivery.DeliveryActivity;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class CartFragment extends Fragment implements DeletAllitemCartInterface {
    private boolean isLogged = false;

    private RecyclerView recyclerView;
    //1 to get data from room
    private LocalDatabase database;

    private TextView sumProdect, discountProdect, totalAll;
    private TextView numberSumProdect, numberDiscountProdect, numberTotalAll;

    private List<MenuItemModel> cartList = new ArrayList<>();
    private Button ok, cancel;

    // will read from api
    private String desconttt = "2";

    private DeleteOneItem deleteOneItem;

    //to call adapter
    private CartRecyclerViewAdapter adapter;

    //check time open resstorant or closed
    private Date dateCompareOne;
    private Date dateCompareTwo;

    final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

    private static String PAGE_ID_KEY = "page_id_key";

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_cart, container, false);

        setUp(v);

        return v;
    }

    private void setUp(View v) {
        apiDiscount(apiService);
        isLogged = SharedPreferencesOfMe.getISLogedDefaults(SharedPreferencesOfMe.IS_LOGED, getContext());
        sumProdect = v.findViewById(R.id.tv_sum_of_otder_manager_booktableManager);
        discountProdect = v.findViewById(R.id.tv_Discount);
        totalAll = v.findViewById(R.id.tv_Total_price);
        numberSumProdect = v.findViewById(R.id.tv_number_sum_of_otder_manager_booktableManager);
        numberDiscountProdect = v.findViewById(R.id.tv_number_Discount);
        numberTotalAll = v.findViewById(R.id.tv__number_Total_price);

        recyclerView = v.findViewById(R.id.recycalViewCart);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        connectLocalDataRoom();




        ok = v.findViewById(R.id.bn_yas);

        // when delete all item from cart
        cancel = v.findViewById(R.id.bn_login);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeletDialog();
            }
        });


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()) {
                    if (isLogged) {
                        if (compareDates()) {
                            if (cartList.size() > 0) {

                                showChoosetheWayDialog();
                            } else {
                                showDailogCart();
                                //Toast.makeText(getContext(), " you dont select item from munu", Toast.LENGTH_SHORT).show();
                            }

                        }
                    } else {
                        showDailogLoghed();
                    }


                } else {
                    showDailogNetworkConnected();
                }

            }
        });


    }

    private void connectLocalDataRoom() {
        //3 to get data from room
        database = LocalDatabase.getAppDatabase(getContext());
        GetOrderAyncTasl getOrderAyncTasl = new GetOrderAyncTasl();


        try {
            //to put the data in adapter
            cartList.addAll(getOrderAyncTasl.execute().get());


            deleteOneItem = new DeleteOneItem() {
                @Override
                public void onClickOneItem(MenuItemModel menuItemModel) {
                    final DeletoneItemAsyncTask deletOneItemAsyncTask = new DeletoneItemAsyncTask();
                    deletOneItemAsyncTask.execute(menuItemModel.getName());
                    cartList.remove(menuItemModel);
                    adapter.setArrayList((ArrayList<MenuItemModel>) cartList);

                    numberSumProdect.setText(ConverDoubleToString.ConDoubleString(TotalFinal.totalPrice(cartList)));
                    numberTotalAll.setText(ConverDoubleToString.ConDoubleString(TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList))));
                }
            };

            adapter = new CartRecyclerViewAdapter(getContext(), deleteOneItem);
            adapter.setArrayList((ArrayList<MenuItemModel>) cartList);

            //to put sum and total
            numberSumProdect.setText(String.valueOf(TotalFinal.totalPrice(cartList)) + " " + FinalString.getJd());
            numberTotalAll.setText(String.valueOf(TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList))) + " " + FinalString.getJd());

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        recyclerView.setAdapter(adapter);
    }


    //2 to get data from room
    private class GetOrderAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }

    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }

    //END get data from room

    private void showChoosetheWayDialog() {
        //show dielog
        ChoosetheWayDialog choosetheWayDialog = new ChoosetheWayDialog();
        choosetheWayDialog.setTypeOrder(new TypeOrder() {
            @Override
            public void onClickOrder(final int id) {


               List<UpdateOrderReqest>updateOrderReqests1=new ArrayList<>();

                double finalPrice = (TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList)));
                String customerId = SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, getContext());

                if (id == 1) {
                    for (int i = 0; i < cartList.size(); i++) {
                        UpdateOrderReqest updateOrderReqest = new UpdateOrderReqest();
                        updateOrderReqest.setItemNameEn(cartList.get(i).getName());
                        updateOrderReqest.setMenuItemId(String.valueOf(cartList.get(i).getId_item()));
                        updateOrderReqest.setFinalPrice(String.valueOf(finalPrice));//cartList.get(i).getPrice()
                        //cartList.get(i).getQuantity();
                        updateOrderReqest.setCustomerId(String.valueOf(customerId));
                        updateOrderReqest.setOrderTypeId("1");
                        updateOrderReqest.setOrderId(String.valueOf(i+1));

                        updateOrderReqests1.add(updateOrderReqest);

                    }

                }
                if (id == 2) {
                    for (int i = 0; i < cartList.size(); i++) {
                        UpdateOrderReqest updateOrderReqest = new UpdateOrderReqest();
                        updateOrderReqest.setItemNameEn(cartList.get(i).getName());
                        updateOrderReqest.setMenuItemId(String.valueOf(cartList.get(i).getId_item()));
                        updateOrderReqest.setFinalPrice(String.valueOf(finalPrice));
                        updateOrderReqest.setCustomerId(String.valueOf(customerId));
                        updateOrderReqest.setOrderTypeId("1");
                        updateOrderReqest.setOrderId(String.valueOf(i+1));

                        updateOrderReqests1.add(updateOrderReqest);

                    }

                }
                if (id == 3) {
                    for (int i = 0; i < cartList.size(); i++) {
                        UpdateOrderReqest updateOrderReqest = new UpdateOrderReqest();
                        updateOrderReqest.setItemNameEn(cartList.get(i).getName());
                        updateOrderReqest.setMenuItemId(String.valueOf(cartList.get(i).getId_item()));
                        updateOrderReqest.setFinalPrice(String.valueOf(finalPrice));
                        updateOrderReqest.setCustomerId(String.valueOf(customerId));
                        updateOrderReqest.setOrderTypeId("1");
                        updateOrderReqest.setOrderId(String.valueOf(i+1));

                        updateOrderReqests1.add(updateOrderReqest);

                    }

                }

              // String vvvvvv=new Gson().toJson(updateOrderReqests1);
                Call<UpdateOrderResponse> call = apiService.updateOrder(updateOrderReqests1);
                call.enqueue(new Callback<UpdateOrderResponse>() {
                    @Override
                    public void onResponse(Call<UpdateOrderResponse> call, Response<UpdateOrderResponse> response) {
                        if (response.body().getResponse().getMessage().equals("order has been updated successfully")) {

                            if (id == 1){
                                Intent intent = new Intent(getContext(), TakeAwayActivity.class);
                            intent.putExtra(PAGE_ID_KEY, 0);
                            startActivity(intent);
                        }
                            else if(id==2){Intent intent = new Intent(getContext(), BookYourTableActivity.class);
                                intent.putExtra(PAGE_ID_KEY, 1);
                                startActivity(intent);}
                            else if(id==3){ //will
                                Intent intent = new Intent(getContext(), DeliveryActivity.class);
                                startActivity(intent);}

                        } else {
                            Toast.makeText(getContext(), "ooo", Toast.LENGTH_SHORT).show();
                            showDailogFailure();

                        }

                    }

                    @Override
                    public void onFailure(Call<UpdateOrderResponse> call, Throwable t) {
                        showDailogFailure();
                    }
                });


            }
        });
        //coz we are in fragmant
        choosetheWayDialog.show(getChildFragmentManager(), "chosethe Way_Fragment");
    }

    private void showDeletDialog() {
        //show dielog
        DeleteAllitemDialog deleteAllitemDialog = new DeleteAllitemDialog();
        deleteAllitemDialog.setButtonsListener(this);
        //coz we are in fragmant
        deleteAllitemDialog.show(getChildFragmentManager(), "deletAllitem_cart_Fragment");
    }


    //deffult dailog
    private void showDailogCart() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.youDontSelectItemFromMunu)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


    //TO delete data from room
    @Override
    public void onClick() {


        database = LocalDatabase.getAppDatabase(getContext());
        DeleteOrderAyncTask deleteOrderAyncTask = new DeleteOrderAyncTask();

        try {
//list
            deleteOrderAyncTask.execute().get();
            cartList.clear();
            adapter.setArrayList((ArrayList<MenuItemModel>) cartList);
            //to delete sum and total
            numberSumProdect.setText(ConverDoubleToString.ConDoubleString(TotalFinal.totalPrice(cartList)));
            numberTotalAll.setText(ConverDoubleToString.ConDoubleString(TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList))));


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class DeleteOrderAyncTask extends AsyncTask<List<MenuItemModel>, Void, Void> {

        @Override
        protected Void doInBackground(List<MenuItemModel>... lists) {
            //list
            return deletOrder(lists);
        }
    }

    private Void deletOrder(List<MenuItemModel>[] lists) {
        database.daoMenu().deleteTable();
        return null;
    }
    //END delete data from room

    //check at internet
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);


        return cm.getActiveNetworkInfo() != null;
    }

    //deffult dailog
    private void showDailogNetworkConnected() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.NetworkConnected)
                .setMessage(R.string.NoooNetworkConnected)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog ad = builder.create();
        ad.show();

    }


    class DeletoneItemAsyncTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            deletoneItem(strings[0]);
            return null;
        }


        private void deletoneItem(String lists) {
            database.daoMenu().deleteItem(lists);

        }
    }


    //check time open resstorant or closed
    private boolean compareDates() {
        Calendar now = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 7);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        dateCompareOne = cal.getTime();

        Calendar ca2 = Calendar.getInstance();
        ca2.set(Calendar.HOUR_OF_DAY, 23);
        ca2.set(Calendar.MINUTE, 00);
        ca2.set(Calendar.SECOND, 0);
        ca2.set(Calendar.MILLISECOND, 0);

        dateCompareTwo = ca2.getTime();

        if (dateCompareOne.before(now.getTime()) && dateCompareTwo.after(now.getTime())) {

            return true;
        }
        showDailogOpetclosedRestorant();
        return false;
    }//End time open...


    //deffult dailog  check time open resstorant or closed
    private void showDailogOpetclosedRestorant() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.soorytime)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }

    //deffult dailog Loghed
    private void showDailogLoghed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.Sorry)
                .setMessage(R.string.pleaceLogin)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.somethingWrong)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }

  //api get Discount
    private void apiDiscount(Endpoint apiService) {

//////////////API
        Call<ReadRetaurantinformationResponse> call1 = apiService.readRetaurantininf();
        ReadOfferRequst readOfferRequst=new ReadOfferRequst();
        readOfferRequst.setOfferId("1");
      Call<ReadOfferResponse> callOffer=apiService.readOffer(readOfferRequst);
      callOffer.enqueue(new Callback<ReadOfferResponse>() {
          @Override
          public void onResponse(Call<ReadOfferResponse> call, Response<ReadOfferResponse> response) {
              List<GetOfferItem> list=response.body().getGetOffer();
              for (int i = 0; i < list.size(); i++) {
                  TotalFinal.numberDiscount =  Double.parseDouble(list.get(i).getOfferPrice());
                  numberDiscountProdect.setText(list.get(i).getOfferPrice());
                  double c=(TotalFinal.sumDiscountPrice((TotalFinal.totalPrice(cartList))));

                  numberTotalAll.setText(ConverDoubleToString.ConDoubleString(c) + " " + "JD");
              }
          }

          @Override
          public void onFailure(Call<ReadOfferResponse> call, Throwable t) {

          }
      });



    }
}



