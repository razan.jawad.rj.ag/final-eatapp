package com.example.eatapp.user_flow.customer.bookYourTable;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.eatapp.R;
import com.example.eatapp.network.models.avaibilityTable.GetDiningTableItem;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TableDialog extends DialogFragment {
    private RecyclerView recyclerViewTable;
    private SwipeRefreshLayout mySwipeRefreshLayout;

    private Button bnYes, bnCansel;
    private List< GetDiningTableItem> tableList = new ArrayList<>();
    private TableRecyclerViewAdapter adapter;
    private SelectTableInterface selectTableInterface;

    public void setSelectTableInterface(SelectTableInterface selectTableInterface) {
        this.selectTableInterface = selectTableInterface;
    }

    public TableDialog() {
    }

    // to send data in dialog
    public static TableDialog newInstance(ArrayList<GetDiningTableItem> tableItemModel) {
        TableDialog frag = new TableDialog();
        Bundle args = new Bundle();
        args.putParcelableArrayList("item",  tableItemModel);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tableList =  getArguments().getParcelableArrayList("item");
        }
    }
    //end send data i dialog

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_table_dialog, container, false);
        setUp(v);
        return v;
    }

    private void setUp(View v) {
        recyclerViewTable = v.findViewById(R.id.recycalView_table);
        bnCansel = v.findViewById(R.id.bn_cancel_table);
        mySwipeRefreshLayout = v.findViewById(R.id.swiperefresh_table);

        recyclerViewTable.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        bnCansel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getList();
                    }
                }
        );

        getList();

    }


    //SwipeRefreshLayout
    private void getList() {
        mySwipeRefreshLayout.setRefreshing(true);

        adapter = new TableRecyclerViewAdapter(selectTableInterface);
        adapter.setArrayList(tableList);
        recyclerViewTable.setAdapter(adapter);
        mySwipeRefreshLayout.setRefreshing(false);


    }


}
