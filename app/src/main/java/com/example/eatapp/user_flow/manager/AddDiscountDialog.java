package com.example.eatapp.user_flow.manager;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.addOffer.AddOfferRequest;
import com.example.eatapp.network.models.addOffer.AddOfferResponse;
import com.example.eatapp.network.models.getOffer.GetOfferItem;
import com.example.eatapp.network.models.getOffer.ReadOfferRequst;
import com.example.eatapp.network.models.getOffer.ReadOfferResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddDiscountDialog extends DialogFragment {
    private TextView tvOldDiscount, tvnumberOldDiscount, tvPreOldDiscount, tvNowDiscount;
    private EditText edNumberNowDiscount;
    private Button bnOK, bnCancel;
    final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

    public AddDiscountDialog() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_discount_dialog, container, false);
        setUP(v);
        return v;
    }

    private void setUP(View v) {
        tvOldDiscount = v.findViewById(R.id.tv_text_current_discount);
        tvnumberOldDiscount = v.findViewById(R.id.tv_current_number_discount);
        // tvPreOldDiscount=v.findViewById(R.id.tv_current_per_discount);
        tvNowDiscount = v.findViewById(R.id.tv_text_now_discount);
        edNumberNowDiscount = v.findViewById(R.id.ed_now_number_discount);
        bnOK = v.findViewById(R.id.bn_yas_add_dicount);
        bnCancel = v.findViewById(R.id.bn_cancel_add_dicount);

        apiDiscount(apiService);
        bnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        bnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if( IsInputValid())
                apiAddOffer();
            }
        });
    }

    private boolean IsInputValid() {
       String number = edNumberNowDiscount.getText().toString();

        if (TextUtils.isEmpty(number)) {

            edNumberNowDiscount.setError("you do not add discount");
            return false;
        }
            return true;
    }

//new offer
    private void apiAddOffer() {
        AddOfferRequest addOfferRequest = new AddOfferRequest();
        addOfferRequest.setOfferId("1");
        String offerPrice = edNumberNowDiscount.getText().toString();
        addOfferRequest.setOfferPrice(offerPrice);
        Call<AddOfferResponse> callAddOffer = apiService.addOffer(addOfferRequest);
        callAddOffer.enqueue(new Callback<AddOfferResponse>() {
            @Override
            public void onResponse(Call<AddOfferResponse> call, Response<AddOfferResponse> response) {
                Toast.makeText(getContext(), "you add discount successfully", Toast.LENGTH_SHORT).show();
                dismiss();
            }

            @Override
            public void onFailure(Call<AddOfferResponse> call, Throwable t) {
                showDailogFailure();
            }
        });
    }

    //curent discunt
    private void apiDiscount(Endpoint apiService) {

        ReadOfferRequst readOfferRequst = new ReadOfferRequst();
        readOfferRequst.setOfferId("1");
        Call<ReadOfferResponse> callOffer = apiService.readOffer(readOfferRequst);
        callOffer.enqueue(new Callback<ReadOfferResponse>() {
            @Override
            public void onResponse(Call<ReadOfferResponse> call, Response<ReadOfferResponse> response) {
                List<GetOfferItem> list = response.body().getGetOffer();
                for (int i = 0; i < list.size(); i++) {
                    tvnumberOldDiscount.setText(list.get(i).getOfferPrice());
                   // TotalFinal.numberDiscount = Double.parseDouble(list.get(i).getOfferPrice());

                }
            }

            @Override
            public void onFailure(Call<ReadOfferResponse> call, Throwable t) {
                showDailogFailure();
            }
        });
    }

    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.FailureApi)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }

}
