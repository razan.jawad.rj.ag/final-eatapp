package com.example.eatapp.user_flow.customer.Login;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "informationUser")
public class LoginDataModel {
    private String fullName;
    private String email;
    private String password;
    private String confirmPassword;
    private String phoneNamber;
    private boolean isMangar;

    @PrimaryKey
    private int id;

    public LoginDataModel(String fullName, String email, String password, String confirmPassword, String phoneNamber, boolean isMangar) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.phoneNamber = phoneNamber;
        this.isMangar = isMangar;
    }

    public LoginDataModel() {

    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPhoneNamber() {
        return phoneNamber;
    }

    public void setPhoneNamber(String phoneNamber) {
        this.phoneNamber = phoneNamber;
    }

    public boolean isMangar() {
        return isMangar;
    }

    public void setMangar(boolean mangar) {
        isMangar = mangar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
