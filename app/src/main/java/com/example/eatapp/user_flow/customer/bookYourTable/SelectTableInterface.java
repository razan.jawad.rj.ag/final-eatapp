package com.example.eatapp.user_flow.customer.bookYourTable;

import com.example.eatapp.network.models.avaibilityTable.GetDiningTableItem;

public interface SelectTableInterface {
    public void onClick(GetDiningTableItem getDiningTableItem);
}
