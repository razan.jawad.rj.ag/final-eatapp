package com.example.eatapp.user_flow.customer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.ClassHelper.TotalFinal;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.getOffer.GetOfferItem;
import com.example.eatapp.network.models.getOffer.ReadOfferRequst;
import com.example.eatapp.network.models.getOffer.ReadOfferResponse;
import com.example.eatapp.network.models.getReservationDateTime.GetReservationDateTimeRequst;
import com.example.eatapp.network.models.getReservationDateTime.GetReservationDateTimeResponse;
import com.example.eatapp.network.models.getReservationDateTime.ReadingReservationCustomer;
import com.example.eatapp.network.models.readDeliveryBill.ReadDeliveryBillRequest;
import com.example.eatapp.network.models.readDeliveryBill.ReadDeliveryBillResponse;
import com.example.eatapp.network.models.readReservationBill.ReadingBill;
import com.example.eatapp.network.models.readReservationBill.ReservationBillReqest;
import com.example.eatapp.network.models.readReservationBill.ReservationBillResponse;
import com.example.eatapp.network.models.readRestaurantInf.ReadRetaurantinformationResponse;
import com.example.eatapp.network.models.readRestaurantInf.ReadingCustomer;
import com.example.eatapp.network.models.read_tackaway_bill.ReadTackawayBillRequest;
import com.example.eatapp.network.models.read_tackaway_bill.ReadTackawayBillResponse;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TakeAwayActivity extends AppCompatActivity {

    private TextView tvNameapp, tvLocationapp, tvTimeOrder, tvTableOrder, tvnumberOrder,
            tvTotal, tvNumberTotal, tvJDtotal, tvDiscount, tvNumberdiscount,
            percentage, tvCashTotal, tvCashNumber, tvJDdicount, done,
            tvHomedelivary, tvHomedelivaryPhone, tvHomedelivaryCity, tvNumOrderBookTable, tvDateBookTable, tvDateBookTableNumber,
            tvTimeBookTable, tvTimeBookTableNumber, tvTableNumBookTable, tvTableNumBookTableNumber,
            tvDelivery, tvNumberServiceBookTable, tvJDserviceBookTable;

    private ImageButton bnStart;
    private ImageButton bnHome;
    private static String PAGE_ID_KEY = "page_id_key";

    private static String TIMETIE = "time";
    private static String DATEDATE = "date";

    //1 to get data from room
    private LocalDatabase database;

    private List<MenuItemModel> cartList = new ArrayList<>();


    private double numberTotal, cashNumber;
    private String customerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_away);

        setup();

    }

    private void setup() {
        Endpoint apiService = ApiClient.getClient().create(Endpoint.class);
        customerId = SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this);

        tvNameapp = findViewById(R.id.tv_name_App);
        tvLocationapp = findViewById(R.id.tv_location_App);
        tvTimeOrder = findViewById(R.id.tv_time_order);
        tvTableOrder = findViewById(R.id.tv_table_order);
        tvnumberOrder = findViewById(R.id.tv_number_order);

        tvTotal = findViewById(R.id.tv_number_total);
        tvNumberTotal = findViewById(R.id.tv_number_total);
        tvJDtotal = findViewById(R.id.tv_JD);
        tvDiscount = findViewById(R.id.tv_discount_finall);
        tvNumberdiscount = findViewById(R.id.tv_num_discount);

        percentage = findViewById(R.id.tv_percentage);
        tvCashTotal = findViewById(R.id.tv_cach_total);
        tvCashNumber = findViewById(R.id.tv_cash_total_number);
        tvJDdicount = findViewById(R.id.tv_JD_tow);
        done = findViewById(R.id.tv_done);

        bnStart = findViewById(R.id.bn_start);
        tvHomedelivary = findViewById(R.id.tv_home_delivary);
        tvHomedelivaryPhone = findViewById(R.id.tv_home_delivary_phone);
        tvHomedelivaryCity = findViewById(R.id.tv_home_delivary_city);

        //Book Table
        tvNumOrderBookTable = findViewById(R.id.tv_num_order_book_table);
        tvDateBookTable = findViewById(R.id.tv_date_book_table);
        tvDateBookTableNumber = findViewById(R.id.tv_date_book_table_num);
        tvTimeBookTable = findViewById(R.id.tv_time_book_table);
        tvTimeBookTableNumber = findViewById(R.id.tv_time_book_table_num);
        tvTableNumBookTable = findViewById(R.id.tv_table_num_book_table);
        tvTableNumBookTableNumber = findViewById(R.id.tv_table_num_book_table_num);
        tvNumberServiceBookTable = findViewById(R.id.tv_number_service_book_table);
        tvJDserviceBookTable = findViewById(R.id.tv_JD_third);
        bnHome = findViewById(R.id.bn_go_to_home);

        //bnStart.setVisibility(View.INVISIBLE);

        //delivery
        tvDelivery = findViewById(R.id.tv_delivery_order);

        if (getIntent().getExtras() != null) {
            int pageId = getIntent().getExtras().getInt("page_id_key");
            if (pageId == 0) {
                tvNumOrderBookTable.setVisibility(View.GONE);
                tvDateBookTable.setVisibility(View.GONE);
                tvDateBookTableNumber.setVisibility(View.GONE);
                tvTimeBookTable.setVisibility(View.GONE);
                tvTimeBookTableNumber.setVisibility(View.GONE);
                tvTableNumBookTable.setVisibility(View.GONE);
                tvTableNumBookTableNumber.setVisibility(View.GONE);


                tvNumberServiceBookTable.setVisibility(View.GONE);
                tvJDserviceBookTable.setVisibility(View.GONE);

                tvDelivery.setVisibility(View.GONE);

                apiTackeAway(apiService);


            } else if (pageId == 1) {
                //book table
                tvTableOrder.setVisibility(View.INVISIBLE);
                tvnumberOrder.setVisibility(View.INVISIBLE);
                tvDelivery.setVisibility(View.GONE);
                tvNumberServiceBookTable.setVisibility(View.GONE);
                tvJDserviceBookTable.setVisibility(View.GONE);


                final ReservationBillReqest reservationBillReqest = new ReservationBillReqest();

                // shareprefrance
                // shareprefrance

                tvNumOrderBookTable.setText("Your Order Number :" +SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this));
                reservationBillReqest.setCustomerId(customerId);

                Call<ReservationBillResponse> call = apiService.reservationBillResponse(reservationBillReqest);
                call.enqueue(new Callback<ReservationBillResponse>() {
                    @Override
                    public void onResponse(Call<ReservationBillResponse> call, Response<ReservationBillResponse> response) {
                        ReadingBill readingBills = response.body().getReadingBill();

                        tvNameapp.setText(readingBills.getRestaurantNameEn());
                        tvLocationapp.setText(readingBills.getCity());
                        //tvDateBookTableNumber.setText(readingBills.getCustomerId());
                        tvHomedelivaryPhone.setText(readingBills.getPhone());
                        tvHomedelivaryCity.setText(readingBills.getCountry());

                    }

                    @Override
                    public void onFailure(Call<ReservationBillResponse> call, Throwable t) {

                    }
                });

                final GetReservationDateTimeRequst getReservationDateTimeRequst = new GetReservationDateTimeRequst();
               getReservationDateTimeRequst.setCustomerId(customerId);
                Call<GetReservationDateTimeResponse> calll = apiService.getReservationDateTimeResponse(getReservationDateTimeRequst);
                calll.enqueue(new Callback<GetReservationDateTimeResponse>() {
                    @Override
                    public void onResponse(Call<GetReservationDateTimeResponse> call, Response<GetReservationDateTimeResponse> response) {
                        ReadingReservationCustomer reservationCustomer = response.body().getReadingReservationCustomer();
                        Date date = null;
//                        tvDateBookTableNumber.setText(reservationCustomer.getReservationStartDate());
                        String dtStart = reservationCustomer.getReservationStartDate();
                        SimpleDateFormat formatRead = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                        try {
                            date = formatRead.parse(dtStart);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        //  Calendar calendar = Calendar.getInstance();

                        if (date != null) {
                            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                            SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
                            String datefrmat = formatDate.format(date);
                            String timefrmat = formatTime.format(date);
                            //tvDateBookTableNumber.setText(datefrmat);
                           // tvTimeBookTableNumber.setText(timefrmat);
                            tvTimeBookTableNumber.setText(getIntent().getExtras().getString(TIMETIE));
                            tvDateBookTableNumber.setText(getIntent().getExtras().getString(DATEDATE));
                        }
                        tvTableNumBookTableNumber.setText(reservationCustomer.getPersonNum());
                    }

                    @Override
                    public void onFailure(Call<GetReservationDateTimeResponse> call, Throwable t) {

                    }
                });
            } else {
                //delivary
                tvNumOrderBookTable.setVisibility(View.GONE);
                tvDateBookTable.setVisibility(View.GONE);
                tvDateBookTableNumber.setVisibility(View.GONE);
                tvTimeBookTable.setVisibility(View.GONE);
                tvTimeBookTableNumber.setVisibility(View.GONE);
                tvTableNumBookTable.setVisibility(View.GONE);
                tvTableNumBookTableNumber.setVisibility(View.GONE);


                tvNumberServiceBookTable.setText("3");

                apiDelivary(apiService);


            }
        }
        bnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TakeAwayActivity.this, CustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 2);
                startActivity(intent);
                finish();
            }
        });
        apiDiscount(apiService);
        apiRestorantINF(apiService);

        connectedLocalDataBase();


    }

    private void apiDiscount(Endpoint apiService) {

        Call<ReadRetaurantinformationResponse> call1 = apiService.readRetaurantininf();
        ReadOfferRequst readOfferRequst=new ReadOfferRequst();
        readOfferRequst.setOfferId("1");
        Call<ReadOfferResponse> callOffer=apiService.readOffer(readOfferRequst);
        callOffer.enqueue(new Callback<ReadOfferResponse>() {
            @Override
            public void onResponse(Call<ReadOfferResponse> call, Response<ReadOfferResponse> response) {
                List<GetOfferItem> list=response.body().getGetOffer();
                for (int i = 0; i < list.size(); i++) {
                    tvNumberdiscount.setText(list.get(list.size() - 1).getOfferPrice());
                }
            }

            @Override
            public void onFailure(Call<ReadOfferResponse> call, Throwable t) {

            }
        });
    }

    private void apiDelivary(Endpoint apiService) {
        final ReadDeliveryBillRequest readDeliveryBillRequest = new ReadDeliveryBillRequest();
        readDeliveryBillRequest.setCustomerId(customerId);
        readDeliveryBillRequest.setEstimateTime("01:00:00");
        Call<ReadDeliveryBillResponse> callDelivery = apiService.readDeliveryBillResponse(readDeliveryBillRequest);
        callDelivery.enqueue(new Callback<ReadDeliveryBillResponse>() {
            @Override
            public void onResponse(Call<ReadDeliveryBillResponse> call, Response<ReadDeliveryBillResponse> response) {
                com.example.eatapp.network.models.readDeliveryBill.ReadingBill readingBill = response.body().getReadingBill();

                tvnumberOrder.setText("Your Order Number :" + readingBill.getCustomerId());
                tvTimeOrder.setText("your order will be ready in: " + readingBill.getEstimateTime());
            }

            @Override
            public void onFailure(Call<ReadDeliveryBillResponse> call, Throwable t) {

            }
        });
    }

    private void apiTackeAway(Endpoint apiService) {
        ReadTackawayBillRequest readTackawayBillRequest = new ReadTackawayBillRequest();
        readTackawayBillRequest.setCustomerId(customerId);
        Call<ReadTackawayBillResponse> call = apiService.readTackawayBillResponseCall(readTackawayBillRequest);
        call.enqueue(new Callback<ReadTackawayBillResponse>() {
            @Override
            public void onResponse(Call<ReadTackawayBillResponse> call, Response<ReadTackawayBillResponse> response) {
                com.example.eatapp.network.models.read_tackaway_bill.ReadingBill readingBill = response.body().getReadingBill();
                tvnumberOrder.setText("Your Order Number :" + readingBill.getCustomerId());
                tvTimeOrder.setText("your order will be ready in: "  + readingBill.getCookingTime());


            }

            @Override
            public void onFailure(Call<ReadTackawayBillResponse> call, Throwable t) {
                showDailogFailure();
            }
        });
    }

    private void apiRestorantINF(Endpoint apiService) {

        Call<ReadRetaurantinformationResponse> call1 = apiService.readRetaurantininf();
        call1.enqueue(new Callback<ReadRetaurantinformationResponse>() {
            @Override
            public void onResponse(Call<ReadRetaurantinformationResponse> call, Response<ReadRetaurantinformationResponse> response) {

                ReadingCustomer readingCustomer = response.body().getReadingCustomer();
                tvNameapp.setText(readingCustomer.getRestaurantNameEn());
                tvLocationapp.setText(readingCustomer.getCity());
                tvHomedelivaryPhone.setText(readingCustomer.getPhone());
                tvHomedelivaryCity.setText(readingCustomer.getCountry());
            }

            @Override
            public void onFailure(Call<ReadRetaurantinformationResponse> call, Throwable t) {

            }
        });
    }

    private void connectedLocalDataBase() {
        //2 to get data from room
        database = LocalDatabase.getAppDatabase(this);
        GetDataOrderAyncTasl getGetDataOrderAyncTasl = new GetDataOrderAyncTasl();

        try {
            cartList.addAll(getGetDataOrderAyncTasl.execute().get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        numberTotal = TotalFinal.totalPrice(cartList);
        tvNumberTotal.setText(ConverDoubleToString.ConDoubleString(numberTotal));
        // tvNumberdiscount.setText(String.valueOf(TotalFinal.totalPrice(cartList)));
        cashNumber = TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList));
        tvCashNumber.setText(String.valueOf(ConverDoubleToString.ConDoubleString(cashNumber)));
        if (getIntent().getExtras() != null) {
            int pageId = getIntent().getExtras().getInt("page_id_key");
            if (pageId == 0) {

                cashNumber = TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList));
                tvCashNumber.setText(ConverDoubleToString.ConDoubleString(cashNumber));

            } else if (pageId == 1) {
                cashNumber = TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList));
                tvCashNumber.setText(ConverDoubleToString.ConDoubleString(cashNumber));
            } else {
                cashNumber = TotalFinal.sumDelivaryPrice(TotalFinal.sumDiscountPrice(TotalFinal.totalPrice(cartList)));
                tvCashNumber.setText(String.valueOf(ConverDoubleToString.ConDoubleString(cashNumber)));
            }
        }

        //tvHomedelivaryPhone.setText(phonr);
    }

    private class GetDataOrderAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }

    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(TakeAwayActivity.this, CustomerActivity.class);
        intent.putExtra(PAGE_ID_KEY, 2);
        startActivity(intent);
        finish();
    }


    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.FailureApi)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


}
