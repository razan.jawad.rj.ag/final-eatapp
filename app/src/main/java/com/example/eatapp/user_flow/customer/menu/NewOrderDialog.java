package com.example.eatapp.user_flow.customer.menu;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.network.models.menu_details.GetStoreDetailsItem;

public class NewOrderDialog extends DialogFragment {

    private TextView tvName, tvPrice, tvQuan, tvNumber;
    private Button bnPlus, bnMinus, bnOk, bnCancel;
    private OnChangeListener mlListener = null;
    private int quantity = 1;
    private static final String KEY_SUPER_STATE = "superState";
    private static final String KEY_NUMINCHES = "quantity";
    private GetStoreDetailsItem menuItemModel;
    private double price;
    private String name;
    private String image;
    private int id;


    private LocalDatabase dataBase;

    public NewOrderDialog() {
    }

    public static NewOrderDialog newInstance(double price, String name, String image,int id) {
        NewOrderDialog frag = new NewOrderDialog();
        Bundle args = new Bundle();
        args.putDouble("price", price);
        args.putString("name", name);
        args.putString("image",image);
        args.putInt("id",id);
        frag.setArguments(args);

        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            price = getArguments().getDouble("price");
            name = getArguments().getString("name");
            image = getArguments().getString("image");
            id=getArguments().getInt("id");

        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_order_dialog, container);
        tvName = v.findViewById(R.id.tv_name_prodact);
        tvPrice = v.findViewById(R.id.tv_price_prodact);/**/
        tvQuan = v.findViewById(R.id.tv_quan_prodact);
        tvNumber = v.findViewById(R.id.tv_number_quan_prodact);
        bnPlus = v.findViewById(R.id.bn_plus_row_cart);
        bnMinus = v.findViewById(R.id.bn_minus);
        bnOk = v.findViewById(R.id.bn_yas);
        bnCancel = v.findViewById(R.id.bn_cancel);
        tvName.setText(name);
        tvPrice.setText((price) + " " + "JD");
        addQuan();

        dataBase = LocalDatabase.getAppDatabase(getContext());

        bnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        bnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddOrderAsyncTak getOrderAyncTask = new AddOrderAsyncTak();
                getOrderAyncTask.execute(new MenuItemModel(name,
                        price, image, quantity,id));
                dismiss();

            }
        });

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get field from view
        tvName = view.findViewById(R.id.tv_name_prodact);
        tvPrice = view.findViewById(R.id.tv_price_prodact);//
        tvQuan = view.findViewById(R.id.tv_quan_prodact);
        tvNumber = view.findViewById(R.id.tv_number_quan_prodact);
        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        tvName.requestFocus();

        getDialog().getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.MATCH_PARENT);

    }


    private void addQuan() {

        updata();
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.bn_plus_row_cart:
                        quantity++;
                        if (mlListener != null)
                            mlListener.onChange(quantity);
                        updata();
                        break;
                    case R.id.bn_minus:
                        quantity--;
                        if (mlListener != null)
                            mlListener.onChange(quantity);
                        updata();
                        break;
                }
            }
        };
        bnPlus.setOnClickListener(listener);
        bnMinus.setOnClickListener(listener);

    }

    private void updata() {
        String text = String.format("%d", quantity);
        if (quantity > 0)
            text = String.format("%d", quantity);
        else {
            text = String.format("%d", quantity);
        }
        tvNumber.setText(text);
        double x = Double.parseDouble(String.valueOf(price)) * quantity;
        tvPrice.setText(String.valueOf(x + " " + "JD"));
        bnMinus.setEnabled(quantity > 1);
    }

    public void setOnChangListener(OnChangeListener listener) {
        mlListener = listener;

    }

    public int getQuantity() {
        return quantity;
    }

    public interface OnChangeListener {
        public void onChange(int length);
    }

    private class AddOrderAsyncTak extends AsyncTask<MenuItemModel, Void, Void> {

        @Override
        protected Void doInBackground(MenuItemModel... lists) {
            addOrder(lists[0]);
            Log.d("database_order", dataBase.daoMenu().toString());
            return null;
        }
    }

    private void addOrder(MenuItemModel menuItemModel) {


        dataBase.daoMenu().insert(menuItemModel);
    }
  /*  @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_SUPER_STATE, super.onSaveInstanceState());
        bundle.putInt(KEY_NUMINCHES, quantity);
        return bundle;
    }



    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            quantity = bundle.getInt(KEY_NUMINCHES);
            super.onRestoreInstanceState(bundle.getParcelable(KEY_SUPER_STATE));
        } else
            super.onRestoreInstanceState(state);
        updata();
    }*/


}
