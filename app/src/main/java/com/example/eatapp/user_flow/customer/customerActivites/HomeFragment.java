package com.example.eatapp.user_flow.customer.customerActivites;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.Login.LoginActivity;
import com.example.eatapp.user_flow.customer.bookYourTable.BookYourTableActivity;
import com.example.eatapp.user_flow.customer.menu.MenuActivity;
import com.example.eatapp.user_flow.manager.ManagerActivity;
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private ImageView imageView;
    private Button menu, bookYourTable, bnCreatAccount, bnSignIn, bnManager, bnLogOut, bnLofout;

    private ViewPager viewPager;
    private WormDotsIndicator wormDotsIndicator;
    private ViewPagerAdapterPcHome adapter;
    private Timer timer;
    private int current_page = 0;
    private static String PAGE_ID_KEY = "page_id_key";


    //check time open resstorant or closed
    private Date dateCompareOne;
    private Date dateCompareTwo;

    private boolean isLogged = false;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        setUp(v);
        return v;

    }

    private void setUp(View v) {
        wormDotsIndicator = v.findViewById(R.id.worm_dots_indicator_pc_home);
        viewPager = v.findViewById(R.id.view_pager_pc_home);
        adapter = new ViewPagerAdapterPcHome(getChildFragmentManager());
        viewPager.setAdapter(adapter);
        wormDotsIndicator.setViewPager(viewPager);

        isLogged = SharedPreferencesOfMe.getISLogedDefaults(SharedPreferencesOfMe.IS_LOGED, getContext());

        viewPager.setCurrentItem(0);

        showPc();


        imageView = v.findViewById(R.id.img_view_home);
        menu = v.findViewById(R.id.bn_menu);
        bookYourTable = v.findViewById(R.id.bn_book_your_table);
        bnCreatAccount = v.findViewById(R.id.bn_creat_account_main);
        bnSignIn = v.findViewById(R.id.bn_sign_in);
        bnLogOut = v.findViewById(R.id.bn_logotycostomer);
        //manager
        bnManager = v.findViewById(R.id.bn_manager);
        bnManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ManagerActivity.class);
                startActivity(intent);
            }
        });

        //end button manger

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()) {
                    Intent intent = new Intent(getContext(), MenuActivity.class);
                    startActivity(intent);
                } else {
                    showDailogNetworkConnected();
                }

            }
        });

        bookYourTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()) {
                    if(isLogged){
                    if (compareDates()) {
                        Intent intent = new Intent(getContext(), BookYourTableActivity.class);
                        intent.putExtra(PAGE_ID_KEY, 4);
                        startActivity(intent);
                    }
                }
                    else {showDailogLoghed();}


                }
                else {
                    showDailogNetworkConnected();
                }
            }
        });

        bnCreatAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()) {
                    LoginActivity.luanhActivity(getContext(), 1);
                } else {
                    showDailogNetworkConnected();
                }
            }
        });
        bnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkConnected()) {
                    //to moving from fragment to anther fragment
                    LoginActivity.luanhActivity(getContext(), 0);
                } else {
                    showDailogNetworkConnected();
                }
            }
        });

        if (isLogged) {
            bnSignIn.setVisibility(View.INVISIBLE);
            bnCreatAccount.setVisibility(View.INVISIBLE);
            bnLogOut.setVisibility(View.VISIBLE);
        } else {
            bnLogOut.setVisibility(View.INVISIBLE);
        }


        bnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesOfMe.setISLogedDefaults(SharedPreferencesOfMe.IS_LOGED, false, getContext());
                bnSignIn.setVisibility(View.VISIBLE);
                bnCreatAccount.setVisibility(View.VISIBLE);
                bnLogOut.setVisibility(View.INVISIBLE);

            }
        });

       bnManager.setVisibility(View.INVISIBLE);
    }

    //to make view pager Pc aoutoSlide
    private void showPc() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (current_page == 4)
                    current_page = 0;
                viewPager.setCurrentItem(current_page++, true);

            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(runnable);
            }
        }, 250, 2500);

    }

    //check at internet
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);


        return cm.getActiveNetworkInfo() != null;
    }

    //deffult dailog
    private void showDailogNetworkConnected() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.NetworkConnected)
                .setMessage(R.string.NoooNetworkConnected)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog ad = builder.create();
        ad.show();

    }


    //check time open resstorant or closed
    private boolean compareDates() {
        Calendar now = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 7);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        dateCompareOne = cal.getTime();

        Calendar ca2 = Calendar.getInstance();
        ca2.set(Calendar.HOUR_OF_DAY, 23);
        ca2.set(Calendar.MINUTE, 00);
        ca2.set(Calendar.SECOND, 0);
        ca2.set(Calendar.MILLISECOND, 0);

        dateCompareTwo = ca2.getTime();

        if (dateCompareOne.before(now.getTime()) && dateCompareTwo.after(now.getTime())) {

            return true;
        }
        showDailogOpetclosedRestorant();
        return false;
    }//End time open...

    //deffult dailog check time open resstorant or closed
    private void showDailogOpetclosedRestorant() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.soorytime)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


    //deffult dailog Loghed
    private void showDailogLoghed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.Sorry)
                .setMessage(R.string.pleaceLogin)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }
}