package com.example.eatapp.user_flow.customer.cart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.FinalString;
import com.example.eatapp.R;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;

public class CartRecyclerViewAdapter extends RecyclerView.Adapter<CartRecyclerViewAdapter.viewitem> {
    private ArrayList<MenuItemModel> items;
    private int change;
    private double preceQuantity;
    private Context context;
    DeleteOneItem deleteOneItem;

    public CartRecyclerViewAdapter(Context context, DeleteOneItem deleteOneItem) {
        this.context = context;
        this.deleteOneItem = deleteOneItem;

    }


    public void setArrayList(ArrayList<MenuItemModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    //onCreateViewHolder used to HAndle on Clicks
    @Override
    public viewitem onCreateViewHolder(final ViewGroup parent, int viewType) {


        //the itemView now is the row


        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_cart, parent, false);


        return new viewitem(itemView);
    }


    //to fill each item with data from the array depending on position
    @Override
    public void onBindViewHolder(viewitem holder, final int position) {
        holder.nameProdect.setText(items.get(position).getName());
        Glide.with(context).load("http://40.76.37.131:8080/rest/rest" + items.get(position).getImage()).into(holder.imageViewCare);

        //   holder.imageProdect.s(items.get(position).getImage());
        change = items.get(position).getQuantity();
        preceQuantity = items.get(position).getQuantity() * items.get(position).getPrice();
        String quantity = String.valueOf(items.get(position).getQuantity());

        holder.paiceProdect.setText(ConverDoubleToString.ConDoubleString(preceQuantity) + " " + FinalString.getJd());
        holder.quantityProdect.setText(quantity);

        //this on click is for the button
        holder.itemView.findViewById(R.id.bn_delete_row_cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteOneItem.onClickOneItem(items.get(position));
            }
        });
    }


    @Override
    public int getItemCount() {
        if (items == null)
            return 0;

        return items.size();
    }

    //The View Item part responsible for connecting the row.xml with
    // each item in the RecyclerView
    //make declare and initalize
    class viewitem extends RecyclerView.ViewHolder {

        //Declare
        TextView nameProdect, paiceProdect, quantityProdect;
        ImageView imageProdect;
        ImageButton delete;
        ImageView imageViewCare;

        //initialize
        public viewitem(View itemView) {
            super(itemView);

            nameProdect = itemView.findViewById(R.id.tv_name_prodact_cart);
            paiceProdect = itemView.findViewById(R.id.tv_price_prodact_cart);
            quantityProdect = itemView.findViewById(R.id.tv_quantity_prodact_cart);
            delete = itemView.findViewById(R.id.bn_delete_row_cart);
            imageViewCare = itemView.findViewById(R.id.tv_img_cart);
            imageProdect = itemView.findViewById(R.id.tv_img_cart);


        }
    }


}
