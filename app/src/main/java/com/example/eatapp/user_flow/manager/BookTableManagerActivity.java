package com.example.eatapp.user_flow.manager;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.eatapp.ClassHelper.ConverDoubleToString;
import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.database.LocalDatabase;
import com.example.eatapp.user_flow.customer.menu.MenuItemModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class BookTableManagerActivity extends AppCompatActivity {
    private LocalDatabase database;
    private List<MenuItemModel> cartList = new ArrayList<>();

    private TextView tvNumberTable, tvLocationTable, tvSumTotal, tvNumberSumTotal;
    private Button bnOk;
    private double numberSumTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_table_manager);

        setUp();

    }

    private void setUp() {
        tvNumberTable = findViewById(R.id.tv_number_table_booktableManager);
        tvLocationTable = findViewById(R.id.tv_location_table_booktableManager);
        tvSumTotal = findViewById(R.id.tv_sum_of_otder_manager_booktableManager);
        tvNumberSumTotal = findViewById(R.id.tv_number_sum_of_otder_manager_booktableManager);

        bnOk = findViewById(R.id.bn_ok_manager_booktableManager);
        connectLocalDataRoom();
        bnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvLocationTable.setText(SharedPreferencesOfMe.gettablePlace(SharedPreferencesOfMe.Table_Place,this));
        tvNumberTable.setText("4");
    }

    private void connectLocalDataRoom() {
        database = LocalDatabase.getAppDatabase(this);
        GetOrderManagerAyncTasl getOrderAyncTasl = new GetOrderManagerAyncTasl();

        try {
            cartList = getOrderAyncTasl.execute().get();
            if (cartList.size() > 0) {

                //to convert double to format  string
                numberSumTotal = 0.0;
                //to put total in text
                tvNumberSumTotal.setText(ConverDoubleToString.ConDoubleString(numberSumTotal) + " " + "JD");

            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class GetOrderManagerAyncTasl extends AsyncTask<Void, Void, List<MenuItemModel>> {

        @Override
        protected List<MenuItemModel> doInBackground(Void... voids) {
            return getOrder();
        }
    }

    private List<MenuItemModel> getOrder() {
        return database.daoMenu().getOrder();
    }
}
