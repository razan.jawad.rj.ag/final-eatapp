package com.example.eatapp.user_flow.customer.bookYourTable;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.getReservationDateTime.GetReservationDateTimeRequst;
import com.example.eatapp.network.models.getReservationDateTime.GetReservationDateTimeResponse;
import com.example.eatapp.network.models.getReservationDateTime.ReadingReservationCustomer;
import com.example.eatapp.network.models.readReservationBill.ReadingBill;
import com.example.eatapp.network.models.readReservationBill.ReservationBillReqest;
import com.example.eatapp.network.models.readReservationBill.ReservationBillResponse;
import com.example.eatapp.user_flow.customer.CustomerActivity;

import java.text.ParseException;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservationBill extends AppCompatActivity {

    private TextView tvNameapp, tvLocationapp, tvNumOrderBookTable, tvDateBookTableNumber,
            tvTimeBookTableNumber, tvTableNumBookTableNumber, tvHomedelivaryPhone, tvHomedelivaryCity;

    private ImageButton bnHome;
    private static String PAGE_ID_KEY = "page_id_key";
    private static String TIMETIE = "time";
    private static String DATEDATE = "date";
    private String customerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservation_bill);
        setUp();
    }

    private void setUp() {
        customerId = SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this);
        tvNameapp = findViewById(R.id.tv_name_App_bill);
        tvLocationapp = findViewById(R.id.tv_location_App_bill);
        tvNumOrderBookTable = findViewById(R.id.tv_num_order_book_table_bill);
        tvDateBookTableNumber = findViewById(R.id.tv_date_book_table_num_bill);
        tvTimeBookTableNumber = findViewById(R.id.tv_time_book_table_num_bill);
        tvTableNumBookTableNumber = findViewById(R.id.tv_table_num_book_table_num_bill);

        tvHomedelivaryPhone = findViewById(R.id.tv_home_delivary_phone_bill);
        tvHomedelivaryCity = findViewById(R.id.tv_home_delivary_city_bill);

        coneactionApi();
        bnHome = findViewById(R.id.bn_go_to_home_bill);

        bnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReservationBill.this, CustomerActivity.class);
                intent.putExtra(PAGE_ID_KEY, 2);
                startActivity(intent);
                finish();
            }
        });
   tvNumOrderBookTable.setVisibility(View.INVISIBLE);
    }

    private void coneactionApi() {

        Endpoint apiService = ApiClient.getClient().create(Endpoint.class);
        final ReservationBillReqest reservationBillReqest = new ReservationBillReqest();

        // shareprefrance
        tvNumOrderBookTable.setText(SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this));
        reservationBillReqest.setCustomerId(SharedPreferencesOfMe.getDefaults(SharedPreferencesOfMe.CUSTOMET_ID, this));
        Call<ReservationBillResponse> call = apiService.reservationBillResponse(reservationBillReqest);
        call.enqueue(new Callback<ReservationBillResponse>() {
            @Override
            public void onResponse(Call<ReservationBillResponse> call, Response<ReservationBillResponse> response) {
                ReadingBill readingBills = response.body().getReadingBill();

                tvNameapp.setText(readingBills.getRestaurantNameEn());
                tvLocationapp.setText(readingBills.getCity());
                ///tvDateBookTableNumber.setText(readingBills.getd);
                tvHomedelivaryPhone.setText(readingBills.getPhone());
                tvHomedelivaryCity.setText(readingBills.getCountry());

            }

            @Override
            public void onFailure(Call<ReservationBillResponse> call, Throwable t) {
                Toast.makeText(ReservationBill.this, getString(R.string.FailureApi), Toast.LENGTH_SHORT).show();
            }
        });
        final GetReservationDateTimeRequst getReservationDateTimeRequst = new GetReservationDateTimeRequst();
        getReservationDateTimeRequst.setCustomerId(customerId);
        Call<GetReservationDateTimeResponse> calll = apiService.getReservationDateTimeResponse(getReservationDateTimeRequst);
        calll.enqueue(new Callback<GetReservationDateTimeResponse>() {
            @Override
            public void onResponse(Call<GetReservationDateTimeResponse> call, Response<GetReservationDateTimeResponse> response) {
                ReadingReservationCustomer reservationCustomer = response.body().getReadingReservationCustomer();
                Date date = null;
//                        tvDateBookTableNumber.setText(reservationCustomer.getReservationStartDate());
                String dtStart = reservationCustomer.getReservationStartDate();
                SimpleDateFormat formatRead = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

                try {
                    date = formatRead.parse(dtStart);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                //  Calendar calendar = Calendar.getInstance();

                if (date != null) {
                    SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
                    String datefrmat = formatDate.format(date);
                    String timefrmat = formatTime.format(date);
                    tvDateBookTableNumber.setText(datefrmat);

                    tvDateBookTableNumber.setText(getIntent().getExtras().getString(DATEDATE));
                    tvTimeBookTableNumber.setText(getIntent().getExtras().getString(TIMETIE));
                }
                tvTableNumBookTableNumber.setText(reservationCustomer.getPersonNum());
            }

            @Override
            public void onFailure(Call<GetReservationDateTimeResponse> call, Throwable t) {
                Toast.makeText(ReservationBill.this, "hhhhhhhhhhhhhhhhhhhh", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ReservationBill.this, CustomerActivity.class);
        intent.putExtra(PAGE_ID_KEY, 2);
        startActivity(intent);
        finish();
    }

}
