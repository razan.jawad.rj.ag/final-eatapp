package com.example.eatapp.user_flow.customer.customerActivites;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.eatapp.R;

public class ViewPagerAdapterPcHome extends FragmentPagerAdapter {


    public ViewPagerAdapterPcHome(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                // ImageViewPagerFragment imageViewPagerFragment = ImageViewPagerFragment.newInstance(R.drawable.pc1);
                return ImageViewPagerFragment.newInstance(R.drawable.pc1);
            case 1:
                return ImageViewPagerFragment.newInstance(R.mipmap.ic_launcher);

            case 2:
                return ImageViewPagerFragment.newInstance(R.drawable.pc3);
            case 3:
                return ImageViewPagerFragment.newInstance(R.drawable.pc0);

            default:
                return ImageViewPagerFragment.newInstance(R.drawable.pc1);

        }
    }

    @Override
    public int getCount() {
        return 0;
    }
}
