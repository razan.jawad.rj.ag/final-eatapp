package com.example.eatapp.user_flow.customer.menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.menu_details.GetStoreDetailsItem;
import com.example.eatapp.network.models.menu_details.MenuDetailsRequest;
import com.example.eatapp.network.models.menu_details.MenuDetailsResponsePojo;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppetisersFragment extends Fragment {


    private SwipeRefreshLayout mySwipeRefreshLayout;
    private RecyclerView recyclerView;
    private MenuRecyclerAdapter adapter;

    public AppetisersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main_courses2, container, false);
        setupViewPager(v);


        return v;
    }


    private void setupViewPager(View v) {
        recyclerView = v.findViewById(R.id.recyclerView_main_courses);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mySwipeRefreshLayout = v.findViewById(R.id.swiperefresh);

        connectionAPI();


        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        connectionAPI();
                    }
                }
        );


    }


    private void connectionAPI() {
        //MenuResponsePojo menuResponsePojo = new MenuResponsePojo();
        final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

        final MenuDetailsRequest menuDetailsRequest = new MenuDetailsRequest();

        menuDetailsRequest.setMenuId("1");

        Call<MenuDetailsResponsePojo> call = apiService.getMenuDetail(menuDetailsRequest);
        call.enqueue(new Callback<MenuDetailsResponsePojo>() {
            @Override
            public void onResponse(Call<MenuDetailsResponsePojo> call, Response<MenuDetailsResponsePojo> response) {
                adapter = new MenuRecyclerAdapter((ArrayList<GetStoreDetailsItem>) response.body().getGetStoreDetails(), getContext());
                recyclerView.setAdapter(adapter);
                mySwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<MenuDetailsResponsePojo> call, Throwable t) {
                Toast.makeText(getContext(), "error in Notwork", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
