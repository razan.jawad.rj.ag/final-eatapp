package com.example.eatapp.user_flow.customer.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.eatapp.R;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

public class LoginActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private static String PAGE_ID_KEY = "page_id_key";
    private AdapterFragmentViewPagerOfLogin adapterFragmentViewPagerOfLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent intent = getIntent();
        int pageId = intent.getIntExtra(PAGE_ID_KEY,0);
        viewPager = findViewById(R.id.view_pager);
        adapterFragmentViewPagerOfLogin =
                new AdapterFragmentViewPagerOfLogin(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapterFragmentViewPagerOfLogin);

        viewPager.setCurrentItem(pageId);
        SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);


    }
    //to moving from fragment to anther fragment
    public static void luanhActivity(Context context, int pageId) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(PAGE_ID_KEY, pageId);
        ((AppCompatActivity) context).startActivity(intent);
       // ((AppCompatActivity) context).finish();
    }
}
