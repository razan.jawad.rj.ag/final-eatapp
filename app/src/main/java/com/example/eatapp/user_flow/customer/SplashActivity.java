package com.example.eatapp.user_flow.customer;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.eatapp.ClassHelper.SharedPreferencesOfMe;
import com.example.eatapp.R;
import com.example.eatapp.user_flow.manager.ManagerActivity;

public class SplashActivity extends AppCompatActivity {
    private boolean isManager = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        isManager = SharedPreferencesOfMe.getISManagerDefaults(SharedPreferencesOfMe.IS_MANAGER, this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isManager == true) {
                    Intent i = new Intent(SplashActivity.this, ManagerActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashActivity.this, CustomerActivity.class);
                    startActivity(i);
                    finish();

                }
            }
        }, 3000);
    }
}
