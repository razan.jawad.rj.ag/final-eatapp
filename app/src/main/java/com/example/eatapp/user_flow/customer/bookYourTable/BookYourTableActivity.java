package com.example.eatapp.user_flow.customer.bookYourTable;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.eatapp.R;
import com.example.eatapp.network.ApiClient;
import com.example.eatapp.network.Endpoint;
import com.example.eatapp.network.models.addReservationCustomer.AddResevationCustomerRequest;
import com.example.eatapp.network.models.addReservationCustomer.AddResevationCustomerResponse;
import com.example.eatapp.network.models.avaibilityTable.AvailabilityTableRequest;
import com.example.eatapp.network.models.avaibilityTable.AvailabilityTableResponse;
import com.example.eatapp.network.models.avaibilityTable.GetDiningTableItem;
import com.example.eatapp.user_flow.customer.TakeAwayActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@TargetApi(Build.VERSION_CODES.O)
@RequiresApi(api = Build.VERSION_CODES.O)

public class BookYourTableActivity extends AppCompatActivity implements SelectTableInterface {
    private TextView tvText, tvDate, tvTime, tvEnterDate, tvEnterTime, tvPerson;
    private EditText tvEnterPerson;
    private String numberPerson = "";
    // private EditText edNumberPerson;
    private Button check_availability;
    private static String PAGE_ID_KEY = "page_id_key";
    private static String TIMETIE = "time";
    private static String DATEDATE = "date";
    // private SwipeRefreshLayout mySwipeRefreshLayout;

    private Calendar startCalender = Calendar.getInstance();
    private Calendar now = Calendar.getInstance();
    private Calendar endCalendar ;

   // private Date date2 = new Date();
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;

    private String apiDateTimeSrart;
    private java.text.SimpleDateFormat readFormaterApi;

    private String time;
    private String date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_your_table);
        setup();
    }

    private void setup() {
       //now.add(Calendar.MINUTE, 30);

        tvText = findViewById(R.id.tv_text_book_table);
        tvDate = findViewById(R.id.tv_date_book_book_table);
        tvTime = findViewById(R.id.tv_time_book_book_table);
        tvPerson = findViewById(R.id.tv_person_book_book_table);
        tvEnterDate = findViewById(R.id.ed_enter_your_date);
        tvEnterTime = findViewById(R.id.ed_enter_your_time);
        tvEnterPerson = findViewById(R.id.ed_enter_your_person);
        check_availability = findViewById(R.id.check_availability);
        tvEnterTime.setEnabled(false);

        //TO PUT CARENT DATE AND TEME IN TEXT VIEW
        java.text.SimpleDateFormat outputFprmatter = new java.text.SimpleDateFormat("yyyy/MM/dd");
        java.text.SimpleDateFormat readFormater = new java.text.SimpleDateFormat("hh:mm aaa");

        tvEnterDate.setText(outputFprmatter.format(now.getTime()));
        tvEnterTime.setText(readFormater.format(now.getTime()));
        //END PUT CARENT DATE AND TEME IN TEXT VIEW

        //TO PUT DATE AND TEME IN API AS THIS FORMATER
        readFormaterApi = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        tvEnterDate.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                try {
                    setDateField();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });




        check_availability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsInputValid()) {
                    if (checkAtDate()) {

                        connectionApiForChechAvalibleTable();


                    }
                }

            }
        });


        tvEnterTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setTimeField();

            }
        });
    }


    //  to do date  Auto-dailog
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setDateField() throws ParseException {


        //format
        final SimpleDateFormat dateFormatprev = new SimpleDateFormat(" dd , MMM  yyyy ");


        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                startCalender.set(year, monthOfYear, dayOfMonth, 0, 0);
                tvEnterDate.setText(dateFormatprev.format(startCalender.getTime()));
                date=dateFormatprev.format(startCalender.getTime());
                setTimeField();
            }

        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));

        //to dont disable previous dates
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        tvEnterTime.setEnabled(true);

        // to calender dates
        datePickerDialog.show();


    }


    //  to do time Auto-dailog
    private void setTimeField() {
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);


        final SimpleDateFormat dateFormatprev = new SimpleDateFormat(" hh:mm aaa");

        timePickerDialog = new TimePickerDialog(BookYourTableActivity.this, new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                startCalender.set(Calendar.HOUR_OF_DAY, selectedHour);
                startCalender.set(Calendar.MINUTE, selectedMinute);
                startCalender.set(Calendar.SECOND, 0);



                tvEnterTime.setText(dateFormatprev.format(startCalender.getTime()));
                time=dateFormatprev.format(startCalender.getTime());
                tvEnterTime.setEnabled(true);


            }
        }, hour, minute, true);//Yes 24 hour time
        timePickerDialog.setTitle("Select Time");


        timePickerDialog.show();


    }


    //to check at edit text person if empty
    private boolean IsInputValid() {
        numberPerson = tvEnterPerson.getText().toString();
        if (numberPerson.isEmpty()) {
            tvEnterPerson.setError(getString(R.string.youmustaddnumberofperson));
            return false;
        }
        else if(chekatNumberPerson().equals("0")){
            showDailogErroePerson();
            return false;}
        return true;
    }


    //check time and person
    private boolean checkAtDate() {


        if (startCalender.compareTo(now) < 0) {
            Log.e("Date1 is after Date2", "time");
            showDailogErrorTime();


            return false;
        } else if (tvEnterPerson.getText().toString().equals("0")) {
            showDailogErroePerson();
            return false;
        }

        return true;
    }

    private String chekatNumberPerson(){
        String number="";
        int x;
        numberPerson=tvEnterPerson.getText().toString();
        x=Integer.parseInt(numberPerson);

        if(x == 0)
        {number="0";}
        else if( x>8)
        {  tvEnterPerson.setError("the max number of persone is 8 ");}

//       else{
//           for (int i = 0; i <numberPerson.length() ; i++) {
//            if(numberPerson.charAt(i)!='0')
//            {number+=numberPerson.charAt(i);}
//            else number="0";
//        }}
    return number;}

    // custom dialog
    private void showTableDialog(List<GetDiningTableItem> tableItemModel) {
        //create dialog
        TableDialog tableDialog = TableDialog.newInstance((ArrayList<GetDiningTableItem>) tableItemModel);
        tableDialog.setSelectTableInterface(this);

        //show dielog
        //1 coz we are in adapter to will do
        FragmentManager fm = this.getSupportFragmentManager();

        //2 show dielog
        tableDialog.show(fm, "NewOrdwr_Fragment");
    }

    //deffult dailog Error Time
    private void showDailogErrorTime() {

        AlertDialog.Builder builder = new AlertDialog.Builder(BookYourTableActivity.this);

        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.youslecttimeinpast)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }

    //deffult dailog
    private void showDailogErroePerson() {

        AlertDialog.Builder builder = new AlertDialog.Builder(BookYourTableActivity.this);

        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.youdonotslectperson)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }


    //connect Api For Chech Avalible Table
    private void connectionApiForChechAvalibleTable() {
        apiDateTimeSrart = readFormaterApi.format(startCalender.getTime());

        AvailabilityTableRequest tableRequest = new AvailabilityTableRequest();
        tableRequest.setReservationStartDate(apiDateTimeSrart);
        tableRequest.setPersonNum(tvEnterPerson.getText().toString());

        Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

        Call<AvailabilityTableResponse> call = apiService.getAvailableTable(tableRequest);
        call.enqueue(new Callback<AvailabilityTableResponse>() {
            @Override
            public void onResponse(Call<AvailabilityTableResponse> call, Response<AvailabilityTableResponse> response) {
                List<GetDiningTableItem> tableItemArrayList = response.body().getGetDiningTable();

                if (tableItemArrayList.size() > 0) {
                    showTableDialog(tableItemArrayList);
                }
            }

            @Override
            public void onFailure(Call<AvailabilityTableResponse> call, Throwable t) {
                Toast.makeText(BookYourTableActivity.this, R.string.FailureApi, Toast.LENGTH_SHORT).show();

            }
        });
    }

    //connect Api For add RESEVATION Table
    @Override
    public void onClick(GetDiningTableItem getDiningTableItem) {

        //use getDiningTableItem to get table info
        //use startCalender to get date and time
        //use shared preferances to get customer id
        //tvPerson
        //send all information to aPI


        AddResevationCustomerRequest addResevationCustomer = new AddResevationCustomerRequest();
        addResevationCustomer.setCustomerId(getDiningTableItem.getTableNum());
        //shared preferances
        addResevationCustomer.setCustomerId("1");
        addResevationCustomer.setPersonNum(numberPerson);
        addResevationCustomer.setReservationStartDate(apiDateTimeSrart);

        endCalendar = (Calendar) startCalender.clone();
        endCalendar.add(Calendar.HOUR_OF_DAY, 3);
        String endTime = readFormaterApi.format(endCalendar.getTime());
        addResevationCustomer.setReservationEndDate(endTime);


        final Endpoint apiService = ApiClient.getClient().create(Endpoint.class);

        Call<AddResevationCustomerResponse> call = apiService.addReservationCustomer(addResevationCustomer);
        call.enqueue(new Callback<AddResevationCustomerResponse>() {
            @Override
            public void onResponse(Call<AddResevationCustomerResponse> call, Response<AddResevationCustomerResponse> response) {
                if (response.body().getResponse().getMessage().equals("Add Customer reservation successfully  ")) {

                    int pageId = getIntent().getExtras().getInt("page_id_key");
                    if (pageId == 4) {

                            Intent intent = new Intent(BookYourTableActivity.this, ReservationBill.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(PAGE_ID_KEY, 1);
                            intent.putExtra(TIMETIE,time);
                            intent.putExtra(DATEDATE,date);
                            BookYourTableActivity.this.startActivity(intent);

                    } else {
                        Intent intent = new Intent(BookYourTableActivity.this, TakeAwayActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(PAGE_ID_KEY, 1);
                        intent.putExtra(TIMETIE,time);
                        intent.putExtra(DATEDATE,date);
                        BookYourTableActivity.this.startActivity(intent);
                    }
                } else {
                    showDailogFailure();
                }
            }

            @Override
            public void onFailure(Call<AddResevationCustomerResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    //deffult dailog Failure
    private void showDailogFailure() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.ohSorry)
                .setMessage(R.string.somethingWrong)
                .setCancelable(false)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        AlertDialog ad = builder.create();
        ad.show();
    }
}



